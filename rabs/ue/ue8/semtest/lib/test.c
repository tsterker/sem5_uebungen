
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "../include/dijkstra.h"


static char *menu = "c - PVinit()\np - P()\nv - V()\ns - Stand()\n\n";

int zeroes[] = {0,0,0,0, 0,0,0,0,0,0,0,0,0,0};  /* initial-werte der semaphoren */

int main()
{
     int c;
     int sem;
     puts(menu);

     while((c = getchar()) != EOF)
     {

	  if(scanf("%d", &sem) == 1)
	  {
	       switch(c)
	       {
	       case 'c':
		    printf("PVinit(%d) = %d\n", sem,  PVinit(sem, zeroes));
		    break;
	       case 'p':
		    printf("P(%d)\n", sem);
		    fflush(stdout);
		    P(sem);
		    break;
	       case 'v':
		    printf("V(%d)\n", sem);
		    fflush(stdout);
		    V(sem);
		    break;
	       case 's':
		    printf("Stand(%d) = %d\n", sem, Stand(sem));
		    break;
	       }
	  }
	  puts(menu);
     }
     return 0;
}
