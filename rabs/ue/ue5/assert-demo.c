#include <assert.h>

int divide(int p, int q)
{
     assert(q != 0);
     return p/q;
}

int main(int argc, char **argv)
{
     divide(1,2);
     divide(1,0);
}
