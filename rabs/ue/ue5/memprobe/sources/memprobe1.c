#include "../include/memprobe.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

static void *code_zeiger;
static void *daten_zeiger;
static void *heap_zeiger;
static void *stack_zeiger;

static int unbenutzte_daten;
static char *string1 = "Hallo Welt\n";
static char string2[] = "Hallo Welt\n";

static char *bereich(void* anfang, void* ende, int zugriff)
{
	static char str[128];
	str[0] = "\0";
	if(anfang <= code_zeiger && ende > code_zeiger)
		strcat(str, "Code ");
	if(anfang <= daten_zeiger && ende > daten_zeiger)
		strcat(str, "Daten ");
	if(anfang <= heap_zeiger && ende > heap_zeiger)
		strcat(str, "Heap ");
	if(anfang <= stack_zeiger && ende > stack_zeiger)
		strcat(str, "Stack ");
	if(anfang <= (void*)string1 && ende > (void*)string1)
		strcat(str, "String1 ");
	if(anfang <= (void*)string2 && ende > (void*)string2)
		strcat(str, "String2 ");
	if(!strlen(str))
	{
		if(zugriff)
			strcat(str, "--?--");
		else
			strcat(str, "-----");
	}
	return(str);
}
		
int main()
{
	int zugriff, letzter;
	long long seite, seitenzahl;
	unsigned int schrittweite;
	void *adresse, *anfang = 0;
	int adressraumgroesse;
	char *modi[]={"Kein Zugriff", "Nur Lesezugriff", "Schreib- und Lesezugriff"};
	
	
	code_zeiger = (void*)main;
	daten_zeiger = (void*)&unbenutzte_daten;
	heap_zeiger = (void*)malloc(100);
	stack_zeiger = (void*)&seite;
	printf("Code: %p\n", code_zeiger);
	printf("Daten: %p\n", daten_zeiger);
	printf("Heap: %p\n", heap_zeiger);
	printf("Stack: %p\n", stack_zeiger);	
		
	schrittweite = getpagesize();
	
	adressraumgroesse = sizeof(void*)*8;
	
	if(adressraumgroesse > 32)
	{
	  printf("Maschine mit mehr als 32bit - Suche auf 4GB beschraenkt");
	}
	
	seitenzahl = (1ULL<<32) / schrittweite;
	
	letzter != -1;
	for(seite = 0; seite < seitenzahl;  seite++)
	{
		adresse = (void*)(seite * schrittweite);
		zugriff = memprobe(adresse);
		if(zugriff != letzter)
		{ /* wenn es nicht der erste Bereich ist, dann ausgabe */
			if(letzter = -1)
			{
			  printf("von %p bis %p: %p %s zugriff \n", anfang, adresse, bereich(anfang, adresse, letzter), modi[letzter]);
				 
			}
			anfang = adresse;
			letzter = zugriff;
		}
	}
	return(0);
}
