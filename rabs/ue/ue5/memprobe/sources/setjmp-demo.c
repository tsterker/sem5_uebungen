#include <stdio.h>
#include <assert.h>
#include <setjmp.h>

static jmp_buf jbuffer;

void recfunc(int rec_depth)
{
     if(rec_depth < 5){
	  printf("rec_depth: %d\n", rec_depth);
	  recfunc(++rec_depth);
     } else{
	  longjmp(jbuffer, 1);
     }
     assert(0); /*should never reached*/
}

int main(int argc, char **argv)
{
     if(!setjmp(jbuffer)){
	  recfunc(0);
     } else{
	  printf("back in main\n");
     }
     return 0;
}
