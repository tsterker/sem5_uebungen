#include <stdio.h>
#include <stdlib.h>
#include "../include/memprobe.h"

int main()
{
	int zugriff, letzter;
	long long seite, seitenzahl;
	unsigned int schrittweite;
	void *adresse, *anfang = 0;
	int adressraumgroesse;
	char *modi[]={"Kein Zugriff", "Nur Lesezugriff", "Schreib- und Lesezugriff"};
	
	schrittweite = getpagesize();
	
	adressraumgroesse = sizeof(void*)*8;
	
	if(adressraumgroesse > 32)
	{
	  printf("Maschine mit mehr als 32bit - Suche auf 4GB beschraenkt");
	}
	
	seitenzahl = (1ULL<<32) / schrittweite;
	
	letzter = -1;
	for(seite = 0; seite < seitenzahl;  seite++)
	{
		adresse = (void*)(seite * schrittweite);
		zugriff = memprobe(adresse);
		if(zugriff != letzter)
		{ /* wenn es nicht der erste Bereich ist, dann ausgabe */
			if(letzter != -1)
			{
			  printf("von %p bis %p: %s \n", anfang, adresse, modi[letzter]);
			}
			anfang = adresse;
			letzter = zugriff;
		}
	}
	return(0);
}
