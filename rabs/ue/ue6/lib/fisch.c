#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <unistd.h>

#include "../include/fisch.h"
#define FILENAME "fischfile.dat"

void print_fischtable(Fischsatz *fs, int fischcount)
{
     int i;
     char *header = "|besitzer\t| fischname\t| preis\t\t|";
     char *hl = "--------------------------------------------------------";
     printf("%s\n", header);
     printf("%s\n", hl);

     for (i = 0; i < fischcount; i++) {
          printf("|%s\t\t| %s\t\t| %d\t\t|\n",fs[i].besitzer,fs[i].fischname, fs[i].preis);
     }
}

void struct_to_file(Fischsatz *fs, int fischcount, char *filename)
{
     int n;
     int filesize = sizeof(Fischsatz)*fischcount;
     FILE *fp;

     if ((fp = fopen(filename,"wb")) != NULL) {
          flockfile(fp);
          n = fwrite(fs,sizeof(Fischsatz),fischcount,fp);
          printf("\n%d bytes successfully written to %s\n",n*sizeof(Fischsatz),filename);
          funlockfile(fp);
          fclose(fp);
     }
}


Fischsatz *file_to_struct(FILE *fp)
{
     int i, n;
     int filesize = file_sizeof(fp);
     int fischcount = filesize/sizeof(Fischsatz);
     Fischsatz *fs = malloc(filesize);
     memset(fs,0,sizeof(Fischsatz)*fischcount);

     rewind(fp);
     n = fread(fs,sizeof(Fischsatz),fischcount,fp);

     return fs;
}

/* Returns file size in BYTES */
int file_sizeof(FILE *fp)
{
     int filesize;
     fseek(fp, 0L, SEEK_END); /* jump to end of file */
     filesize = ftell(fp);    /* current byte of file == filesize */
     rewind(fp);              /* jump to beginning of file */
     return filesize;
}

void print_from_besitzer(Fischsatz *fs, int fcount, char *besitzer)
{
     int i;
     printf("Fische von %s:\n", besitzer);
     printf("|Name\t\t|Preis|\n");

     for(i = 0; i < fcount; i++){
          if(strcmp(fs[i].besitzer, besitzer) == 0){
               printf("%s\t|%d\n", fs[i].fischname, fs[i].preis);
          }
     }
}


int gibts_schon(Fischsatz *fs, int fcount, char *besitzer, char *fischname)
{
     int i;
     for(i = 0; i < fcount; i++){
          if(strcmp(fs[i].besitzer, besitzer) == 0 && strcmp(fs[i].fischname, fischname) == 0){
               return 1;
          }
     }
     return 0;
}


void append(Fischsatz *fs, char *filename)
{
     FILE *fp;
     if ((fp = fopen(filename,"a")) != NULL) {
          fwrite(fs,sizeof(Fischsatz),1,fp);
          fclose(fp);
     }
}

/* fs ist ein array von Fischsatz */
/* returns 0 if no change was made */
int price_up(Fischsatz *fs, int fischcount, char *besitzer, int preis)
{
     int i;
     int changed = 0;
     int besitzer_exists = 0;

     for (i = 0; i < fischcount; i++){
          if(strcmp(fs[i].besitzer, besitzer) == 0){
               besitzer_exists = 1;
               printf("%s alter preis: %d\n",fs[i].fischname,  fs[i].preis);
               fs[i].preis += preis;
               printf("%s neuer preis: %d\n",fs[i].fischname,  fs[i].preis);
               changed = 1;
          }
     }
     if (! besitzer_exists){
          printf("Besitzer '%s' existiert nicht.\n", besitzer);
     }
     return changed;
}

int checkout_besitzer(Fischsatz *fs, int fischcount, char *besitzer)
{
     int i;
     int sum = 0;
     int besitzer_exists = 0;

     for (i = 0; i < fischcount; i++){
          if(strcmp(fs[i].besitzer, besitzer) == 0){
               besitzer_exists = 1;
               sum += fs[i].preis;
          }
     }
     if (besitzer_exists)
          printf("%s hat zu zahlen: %d\n", besitzer, sum);
     else
          printf("Besitzer '%s' existiert nicht.\n", besitzer);
     return sum;
}

char *delete_besitzer(Fischsatz *fs, int fischcount, char *besitzer)
{
     FILE *fp;
     int i;
     char *filename = "tmp";

     if ((fp = fopen(filename,"a")) != NULL) {

          for (i = 0; i < fischcount; i++){
               if(strcmp(fs[i].besitzer, besitzer) != 0){
                    /* TODO struct array richtig machen!? */
                    fwrite(&fs[i],sizeof(Fischsatz),1,fp);
               }
          }
          fclose(fp);
     }
     return filename;
}

void print_help(char *filename)
{
     printf("Usage:\n");
     printf("-------------------------------------------------------\n");
     printf("./%s -l\t\t\t\tTabellle mit allen Fischsaetzen\n", filename);
     printf("./%s -l <besitzer>\t\t\tTabelle mit allen Fischaetzen eines bestimmten Besitzers\n", filename);
     printf("./%s -n <besitzer> <fischname>\tNeuen Fisch fuer bestimmten Besitzer anlegen\n", filename);
     printf("./%s -z <besitzer> <preis>\t\tDen Preis bei allen Fischen von <besitzer> um <preis> erhoehen.\n", filename);
     printf("./%s -d <besitzer>\t\t\tDen von <besitzer> zu zahlenden Gesamtbetrag berechnen und alle fischsaetze von ihm loeschen.\n", filename);
}

int main(int argc, char **argv)
{
     char *op;
     char *opval;
     char *tmpname;
     int i;
     int preis;

     char *besitzer, *fischname;

     /* TODO schicker machen? */
     FILE *fp = fopen(FILENAME, "rb");
     if (fp == NULL){
          fp = fopen(FILENAME,"w+");
     }

     int fischcount = file_sizeof(fp)/sizeof(Fischsatz);
     Fischsatz *fs = file_to_struct(fp);
     Fischsatz *new = malloc(sizeof(Fischsatz));

     if (argc > 1){
          op = argv[1];
          if (strncmp(op, "-l", 2) == 0){
               if(argc > 2){
                    opval = argv[2];
                    print_from_besitzer(fs, fischcount, opval);
               }
               else
                    print_fischtable(fs, fischcount);
          }
          else if (strncmp(op, "-n", 2) == 0){
               if (argc > 3){
                    besitzer = argv[2];
                    fischname = argv[3];

                    /* TODO: abfrage ob schon vorhanden!! */
                    if (gibts_schon(fs, fischcount, besitzer, fischname)){
                         printf("Besitzer-Fischname Kombination schon vorhanden\n");

                    }
                    else if (strlen(besitzer) < MAX_NAME && strlen(fischname) < MAX_NAME){
                         strcpy(new->besitzer, argv[2]);
                         strcpy(new->fischname, argv[3]);
                         new->preis = DEFAULT_PRICE;
                         append(new, FILENAME);
                    }
                    else
                         printf("Zu langer Name, erlaubt: %d Zeichen\n", MAX_NAME-1); /* -1 wegen '\0' */
               }
               else{
                    printf("usage:\n-l <besitzer> <fischname>\n");
               }

          }
          else if (strncmp(op, "-z", 2) == 0){
               if (argc > 3){
                    preis = strtoul(argv[3], 0, 10);

                    /* only if price is valid code after && will be executed */
                    if( preis && price_up(fs, fischcount, argv[2], preis) != 0)
                         struct_to_file(fs, fischcount, FILENAME);
               }
               else{
                    printf("usage:\n-z <besitzer> <preis>\n");
               }
          }
          else if (strncmp(op, "-d", 2) == 0){
               if (argc > 2){
                    if( checkout_besitzer(fs, fischcount, argv[2]) ){
                         tmpname = delete_besitzer(fs, fischcount,  argv[2]);
                         rename("tmp", FILENAME);
                    }
               }
               else{
                    printf("usage:\n-d <besitzer>\n");
               }
          }
          else{
               print_help(argv[0]);
          }
     }
     free(fs);
     free(new);
     fclose(fp);

     return 0;
}
