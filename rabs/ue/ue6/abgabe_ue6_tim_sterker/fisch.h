
#include <stdio.h>


#ifndef __FISCHDEF_H__
#define __FISCHDEF_H__

#define MAX_NAME 20
#define DEFAULT_PRICE 17


typedef struct fis{
     char besitzer[MAX_NAME];
     char fischname[MAX_NAME];
     int preis;
} Fischsatz;


void struct_to_file(Fischsatz *fs, int fischcount, char *filename);
Fischsatz *file_to_struct(FILE *fp);
int file_sizeof(FILE *fp);

void print_fischsatz(Fischsatz *fs, int size);
void print_fischtable(Fischsatz *fs, int fischcount);
void print_from_besitzer(Fischsatz *fs, int fcount, char *besitzer);

int gibts_schon(Fischsatz *fs, int fcount, char *besitzer, char *fischname);
void append(Fischsatz *fs, char *filename);
int price_up(Fischsatz *fs, int fischcount, char *besitzer, int preis);
int checkout_besitzer(Fischsatz *fs, int fischcount, char *besitzer);
char *delete_besitzer(Fischsatz *fs, int fischcount, char *besitzer);

#endif /*__FISCHDEF_H__*/
