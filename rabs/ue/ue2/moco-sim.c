/**
 ** MoCo -- Model Computer Simulation
 **
 ** Usage: moco-sim <program.exe>
 **
 ** Prof. Dr. Robert Kaiser
 ** Hochschule RheinMain, DCSM
 **
 **
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "moco-opcodes.h"
#include "moco-mem.h"


#define NUM_REGISTERS  16

#define STACK_POINTER 15

static struct ProcessorState
{
    int Register[NUM_REGISTERS];
    int PC;
    enum { NEG, POS, ZERO} F;;
} Processor;


/** Thow an exception: for now this just terminates the processor */
static void exception(char* text)
{
    printf("******* Exception: %s *******\n", text);
    exit(1);
}

/** Update flags according to last arithmetic result */
static void update_F(int result)
{
    if(result < 0)
        Processor.F = NEG;
    else if (result > 0)
        Processor.F = POS;
    else /** if (result == 0) */
        Processor.F = ZERO;
}

/** Stop instruction: Halt processor */
static void I_halt(void)
{
    printf("******* Program ended *******\n");
}


/** In instruction: Read decimal number into register */
static void I_in(int s, int rt)
{
    static int new_line = 1;
    if(s == 0) /** decimal number input */
    {
        printf("In <%d>", s);
        fflush(stdout);
        if(scanf("%d", &Processor.Register[rt])!= 1)
	    fflush(stdin);
    }
    else if(s == 1) /** text input */
    {   /** print prompt only if beginning a new line */
	if(new_line)
	{
	    printf("In <%d>", s);
	    fflush(stdout);
	    new_line = 0;
	}
        if((Processor.Register[rt] = getchar()) == '\n')
	{
	    Processor.Register[rt] = '\0';
	    new_line = 1;
	}
    }
}

/** Out instruction: Output register contents */
static void I_out(int rs, int s)
{
    static int new_line = 1;
    if(s == 0) /** decimal number input */
    {
	printf("Out <%d> %d\n", s, Processor.Register[rs]);
    }
    else if(s == 1) /** text output */
    {
        if(new_line)
	{   /** print prompt only if a new line has been started */
	    printf("Out <%d> %c", s, Processor.Register[rs]);
	    new_line = 0;
	}
	else
	{
	    putchar(Processor.Register[rs]);
            if(Processor.Register[rs] == '\n')
	      new_line = 0;
	}
    }
}


/** Ldn instruction: Load constant into register */
static void I_ldn(int k, int rt)
{
    if(rt < 0 || rt >= NUM_REGISTERS)
    {
        exception("invalid register!");
    }
    update_F(Processor.Register[rt] = k);
}

/** Add instruction: adds contents of registers rs1 and rs2,
    stores result into register rt (rt = rs1 + rs2) */
static void I_add(int rs1, int rs2, int rt)
{
    if(rs1 < 0 || rs1 >= NUM_REGISTERS ||
       rs2 < 0 || rs2 >= NUM_REGISTERS ||
       rt  < 0 || rt  >= NUM_REGISTERS)
    {
        exception("invalid register!");
    }
    update_F(Processor.Register[rt] = 
             Processor.Register[rs1] + Processor.Register[rs2]);
}

/** Sub instruction: subtracts contents of register rs2 from rs1,
    stores result in register rt (rt = rs1 - rs2) */
static void I_sub(int rs1, int rs2, int rt)
{
    if(rs1 < 0 || rs1 >= NUM_REGISTERS ||
       rs2 < 0 || rs2 >= NUM_REGISTERS ||
       rt  < 0 || rt  >= NUM_REGISTERS)
    {
        exception("invalid register!");
    }
    update_F(Processor.Register[rt] = 
             Processor.Register[rs1] - Processor.Register[rs2]);
}

/** Mul instruction: multiplies contents of registers rs1 and rs2,
    stores result in register rt (rt = rs1 * rs2) */
static void I_mul(int rs1, int rs2, int rt)
{
    if(rs1 < 0 || rs1 >= NUM_REGISTERS ||
       rs2 < 0 || rs2 >= NUM_REGISTERS ||
       rt  < 0 || rt  >= NUM_REGISTERS)
    {
        exception("invalid register!");
    }
    update_F(Processor.Register[rt] = 
             Processor.Register[rs1] * Processor.Register[rs2]);
}

/** Div instruction: divides contents of register rs1 by rs2,
    stores result in register rt (rt = rs1 / rs2) */
static void I_div(int rs1, int rs2, int rt)
{
    if(rs1 < 0 || rs1 >= NUM_REGISTERS ||
       rs2 < 0 || rs2 >= NUM_REGISTERS ||
       rt  < 0 || rt  >= NUM_REGISTERS)
    {
        exception("invalid register!");
    }
    else if(Processor.Register[rs2] == 0)
    {
        exception("division by zero!");
    }
    update_F(Processor.Register[rt] = 
             Processor.Register[rs1] / Processor.Register[rs2]);
}

/** Addn instruction: adds constant k to register rs,
    stores result in register rt (rt = rs + k) */
static void I_addn(int rs, int k, int rt)
{
    if(rs < 0 || rs >= NUM_REGISTERS ||
       rt < 0 || rt  >= NUM_REGISTERS)
    {
        exception("invalid register!");
    }
    update_F(Processor.Register[rt] = Processor.Register[rs] + k);
}

/** Subn instruction: subtracts constant k from register rs,
    stores result in register rt (rt = rs - k) */
static void I_subn(int rs, int k, int rt)
{
    if(rs < 0 || rs >= NUM_REGISTERS ||
       rt < 0 || rt >= NUM_REGISTERS)
    {
        exception("invalid register!");
    }
    update_F(Processor.Register[rt] = Processor.Register[rs] - k);
}

/** Muln instruction: multipliies constant k and register rs,
    stores result in register rt (rt = rs * k) */
static void I_muln(int rs, int k, int rt)
{
    if(rs < 0 || rs >= NUM_REGISTERS ||
       rt < 0 || rt >= NUM_REGISTERS)
    {
        exception("invalid register!");
    }
    update_F(Processor.Register[rt] = Processor.Register[rs] * k);
}

/** Div instruction: divides contents of register rs by constant k,
    stores result in register rt (rt = rs / k) */
static void I_divn(int rs, int k, int rt)
{
    if(rs < 0 || rs >= NUM_REGISTERS ||
       rt < 0 || rt >= NUM_REGISTERS)
    {
        exception("invalid register!");
    }
    else if(k == 0)
    {
        exception("Division durch Null!");
    }
    update_F(Processor.Register[rt] = Processor.Register[rs] / k);
}


/** Jmp instruction: jump to address */
static void I_jmp(int a)
{
    Processor.PC = a;
}

/** Jmpz instruction: jump to address if F = Zero  */
static void I_jmpz(int a)
{
    if(ZERO == Processor.F)
        Processor.PC = a;
}

/** Jmpnz instruction: jump to address if F != Zero  */
static void I_jmpnz(int a)
{
    if(ZERO != Processor.F)
        Processor.PC = a;
}

/** Jmpp instruction: jump to address if F = POS */
static void I_jmpp(int a)
{
    if(POS == Processor.F)
        Processor.PC = a;
}

/** Jmpn instruction: jump to address if F = NEG */
static void I_jmpn(int a)
{
    if(NEG == Processor.F)
        Processor.PC = a;
}

static void I_load(int ra, int rt)
{
     int ma;

     if(rt < 0 || rt >= NUM_REGISTERS ||
	ra < 0 || ra >= NUM_REGISTERS)
     {
	  exception("invalid register!");
     }
     else{

	  ma = Processor.Register[ra];

	  if(M_DataAccessWord(ma)){
	       Processor.Register[rt] = M_ReadDataWord(ma);
	  }
	  else{
	       exception("Invalid data memory adress!");
	  }
     }
}


static void I_loadb(int ra, int ro, int rt)
{

     /* memory adress */
     int ma;

     if(rt < 0 || rt >= NUM_REGISTERS ||
	ra < 0 || ra >= NUM_REGISTERS ||
	ro < 0 || ro >= NUM_REGISTERS)
     {
	  exception("invalid register!");
     }
     else{

	  ma = Processor.Register[ra] + Processor.Register[ro];

	  if(M_DataAccessByte(ma)){
	       Processor.Register[rt] = M_ReadDataByte(ma);
	  }
	  else{
	       exception("Invalid data memory adress!");
	  }
     }
}

static void I_stor(int rs, int ra)
{
     int ma;

     if(rs < 0 || rs >= NUM_REGISTERS ||
	ra < 0 || ra >= NUM_REGISTERS)
     {
	  exception("invalid register!");
     }
     else{

	  ma = Processor.Register[ra];

	  if(M_DataAccessWord(ma)){
	       M_WriteDataWord(Processor.Register[ma], Processor.Register[rs]);
	  }
	  else{
	       exception("Invalid data memory adress!");
	  }
     }
}

static void I_storb(int rs, int ra, int ro)
{
     int ma;

     if(rs < 0 || rs >= NUM_REGISTERS ||
	rs < 0 || rs >= NUM_REGISTERS ||
	ro < 0 || ro >= NUM_REGISTERS)
     {
	  exception("invalid register!");
     }
     else{

	  ma = Processor.Register[ra] + Processor.Register[ro];

	  if(M_DataAccessByte(ma)){
	       M_WriteDataByte(Processor.Register[ma], Processor.Register[rs]);
	  }
	  else{
	       exception("Invalid data memory adress!");
	  }
     }
}

static void I_push(int rs)
{
     if(rs < 0 || rs >= NUM_REGISTERS)
     {
	  exception("invalid register!");
     }
     else{
	  int stk_ptr = Processor.Register[STACK_POINTER] - sizeof(int);

	  if(M_StackAccessWord(stk_ptr)){
	       M_WriteStackWord(stk_ptr, Processor.Register[rs]);
	       Processor.Register[STACK_POINTER] = stk_ptr;
	  }
	  else{
	       exception("[PUSH] Invalid data memory adress!");
	  }
     }
}


static void I_pop(int rt)
{
     if(rt < 0 || rt >= NUM_REGISTERS)
     {
	  exception("invalid register!");
     }
     else{
	  int stk_ptr = Processor.Register[STACK_POINTER];

	  if(M_StackAccessWord(stk_ptr)){
	       Processor.Register[rt] = M_ReadStackWord(stk_ptr);
	       Processor.Register[STACK_POINTER] = stk_ptr + sizeof(int);
	  }
	  else{
	       exception("[POP] Invalid data memory adress!");
	  }
     }
}


static void I_jsr(int a)
{
     int stack_ptr_address = Processor.Register[STACK_POINTER] - sizeof(int);
     if(M_StackAccessWord(stack_ptr_address)){
	  Processor.Register[STACK_POINTER] = stack_ptr_address;
	  M_WriteStackWord(stack_ptr_address, Processor.PC);
	  Processor.PC = a;
     }
     else{
	  exception("[JSR] Invalid data memory adress!");
     }
}


static void I_ret(void)
{
     int stack_ptr_address = Processor.Register[STACK_POINTER];

     if(M_StackAccessWord(stack_ptr_address)){
	  Processor.PC = M_ReadStackWord(stack_ptr_address);
	  Processor.Register[STACK_POINTER] = stack_ptr_address + sizeof(int);
     }
     else{
	  exception("[RET] Invalid data memory adress!");
     }
}


struct instruction
{
    void (*instruction)();
    int num_ops;

};

struct instruction InstructionSet[NUM_INSTRUCTIONS] = 
{
    { I_halt,     0},
    { I_in,       2},
    { I_out,      2},
    { I_ldn,      2},
    { I_add,      3},
    { I_sub,      3},
    { I_mul,      3},
    { I_div,      3},
    { I_addn,     3},
    { I_subn,     3},
    { I_muln,     3},
    { I_divn,     3},
    { I_jmp,      1},
    { I_jmpz,     1},
    { I_jmpnz,    1},
    { I_jmpp,     1},
    { I_jmpn,     1},
    { I_load,     2},
    { I_loadb,    3},
    { I_stor,     2},
    { I_storb,    3},
    { I_push,     1},
    { I_pop,      1},
    { I_jsr,      1},
    { I_ret,      0}
};


/** The Processor: */
static void TheProcessor()
{
    int OpCode;
    void (*Instruction)();
    int Operands[3];
    int NumOperands;
    int i;

    Processor.PC = 0;

    while(1)
    {
        if(!M_ProgramAccessWord(Processor.PC))
        {
            printf("PC=%d\n", Processor.PC);
            exception("invalid memory address!");
        }

        /** fetch next instruction */
        OpCode = M_ReadProgramWord(Processor.PC);
	Processor.PC += sizeof(int);
	if(OpCode < 0 || OpCode >= NUM_INSTRUCTIONS)
        {
            exception("invalid opcode!");
        }
        /** Ermittle Anzahl Operands (max. 3) */
        NumOperands = InstructionSet[OpCode].num_ops;
        if(NumOperands > 3)
        {
            exception("invalid number of operands!");
            exit(1);
        }

        /** Fetch operands */
        for(i = 0; i < NumOperands; i++)
	{
	    if(!M_ProgramAccessWord(Processor.PC))
	    {
		printf("PC=%d\n", Processor.PC);
		exception("invalid memory address!");
	    }
            Operands[i] = M_ReadProgramWord(Processor.PC);
            Processor.PC += sizeof(int);
	}
        /** Execute instruction */
        Instruction = InstructionSet[OpCode].instruction;
        Instruction(Operands[0], Operands[1], Operands[2]);
        if(OpCode == HALT)
            return;
    }
}

int main(int argc, char *argv[])
{
    struct stat statbuf;

    if(argc != 2)
    {
        printf("Usage: moco-sim <program file>\n");
        exit(1);
    }
    if(stat(argv[1], &statbuf) != 0)
    {
        perror("stat");
        exit(1);
    }
    if(M_CreateSpace(argv[1], statbuf.st_size) < 0)
        return 1;
    TheProcessor();
    M_DeleteSpace(0);
    return 0;
}

