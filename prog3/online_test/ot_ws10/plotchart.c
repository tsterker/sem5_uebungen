#include <stdio.h>
#include <stdlib.h>
#include <string.h>



#define H 20
#define B 20

char **canvas;


int check_chart_pos(int x, int width, int height)
{
     int i, j;
     for(i = 0; i < H; i++){
	  if(i >= H-height){
	       for(j = 0; j < B; j++){
		    if(j >= x && j < x+width)
			 if(canvas[i][j] != '.')
			      return 0;
		    
	       }
	  }
     }
     return 1;
}


void plot_chart(int x, int width, int height, char c)
{
     int i, j;
     for(i = 0; i < H; i++){
	  if(i >= H-height){
	       for(j = 0; j < B; j++){
		    if(j >= x && j < x+width)
			 canvas[i][j] = c;
		    
	       }
	  }
     }
}

void set_char(char *c)
{
     
     *c = '.';

}

void loop_canvas(void (*func)(char *))
{
     int i, j;
     
     for(i = 0; i < H; i++){
	  for(j = 0; j < B; j++){
	       func(&canvas[i][j]);
	  }
     }
}



void init_canvas()
{
     int i;

     canvas = (char**)malloc(H*sizeof(char*));

     for(i = 0; i < B; i++){
	  canvas[i] = (char*)calloc(B, sizeof(char));
     }
}

void fill_canvas(char c)
{
     

}


void clear_canvas()
{
     int i;


     for(i = 0; i < B; i++){
	  free(canvas[i]);
     }
     free(canvas);
}

void print_canvas()
{
     int i, j;
     
     for(i = 0; i < H; i++){
	  for(j = 0; j < B; j++){
	       printf("%c ", canvas[i][j]);
	  }
	  printf("\n");
     }
}


int main(int argc, char **argv)
{
     init_canvas();
     loop_canvas(set_char);
     plot_chart(3, 5, 5, 'X');
     plot_chart(9, 3,  19, 'X');


     if(check_chart_pos(13, 5, 5))	  
	  printf("FREI!\n");
     else
	  printf("NOT FREI!\n");



     plot_chart(13, 5, 5, 'X');
     
     if(check_chart_pos(13, 5, 5))	  
	  printf("FREI!\n");
     else
	  printf("NOT FREI!\n");


     plot_chart(18, 100, 100, 'O');


     print_canvas();
     clear_canvas();
     

     if(argc == 2){
	  

     }
     else{
	  

     }

     
     
     return 0;
}


