#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>


char *lv ="aeiou";
char *uv ="AEIOU";


int isv(const char *str, char *v)
{
     while(*v)
	  if(*str == *v++)
	       return 1;
     return 0;
}

int delv(char *str, char *v)
{
     int count = 0;
     char *tmp = str;
     while(*(tmp = str))
	  if(isv(str, v) && ++count)
	     while(*tmp)
		  *tmp++ = *(tmp+1);
	  else
	       str++;
     return count;
}

int str_del_lowervowels(char *str)
{
     return delv(str, lv);

}

int str_del_uppervowels(char *str)
{
     return delv(str, uv);
}

int str_del_vowels(char *str)
{
     int i = 0;
     i += str_del_lowervowels(str);
     i += str_del_uppervowels(str);
     return i;
}

int main(int argc, char **argv)
{
     char *str;

     if(argc == 2){
	  str = (char*)malloc(strlen(argv[1])*sizeof(char));
	  strcpy(str, argv[1]);
	  str_del_vowels(str);
	  printf("%s\n", str);
	  
     }
     else{

     }
     
     return 0;
}


