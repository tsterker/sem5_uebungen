#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>




double e = 0.0000001;

double heron(double w, double x)
{
     return (w + (x/w))/2;
}


int main(int argc, char **argv)
{
     double w, x, last = 0;
     
     if(2 != argc){
	  return 0;
	  
     }
     
     x = atof(argv[1]);
     w = heron(1, x);
     
     while(fabs(last-w) > e){
	  last = w;
	  w = heron(w, x);
	  
     }
     printf("sqrt(%f) == %f\n", x, w);
     

     
     return w;
}


