#include <stdio.h>
#include <stdlib.h>
#include <string.h>



struct node
{
     struct node *left;
     struct node *right;
     int count;
     char *str;
};

typedef struct node *pnode;


int highest_count = 0;

int empty(pnode root)
{
     return (root == NULL);
}

pnode insert(pnode root, char *insert)
{
     pnode tmp_root = root;
     pnode parent = root;
     int diff;
     
     pnode new = (pnode)malloc(sizeof(struct node));
     new->str = (char*)malloc(30*sizeof(char));
     new->str = insert;
     printf("inserting : %s ", new->str);
     new->count = 1;
     highest_count = (0 == highest_count)? 1 : highest_count; /* if 0=> 1 else same */
     new->left = NULL;
     new->right = NULL;
     
     if(empty(root)){
	  root = new;
	  root->str = insert;
	  return root;
     }

     while(tmp_root){
	  diff = strcmp(insert, tmp_root->str);
	  parent = tmp_root;
	  if(0 == diff){
	       printf(" == root\n");
	       tmp_root->count++;
	       highest_count = (highest_count < tmp_root->count) ? tmp_root->count : highest_count;
	       diff = strcmp(parent->str, tmp_root->str);
	       if(diff < 0){
		    parent->left = tmp_root;
	       printf(" <== %s\n", parent->str);
	       }
	       else{
		    printf(" %s ==>\n", parent->str);
		    parent->right = tmp_root;
	       }

	       return root;
	  }
	  else if(diff > 0){
	       tmp_root = tmp_root->right;
	  }
	  else{
	       tmp_root = tmp_root->left;
	  }
     } /* first occurence */
     
     printf("\n");
     
     if(diff > 0){
	  parent->right = new;
     }
     else{
	  parent->left = new;
     }
     return root;
}


void traverse(pnode root)
{
     pnode tmp = root;
     pnode parent;

     printf("rein\n");
     if(! root->left && ! root->right){
	  return;
     }
     
	       parent = tmp;

	       if(tmp->count >= highest_count){
		    printf("%d:%s\n", tmp->count, tmp->str);
	       }else{
		    printf("%d:%s\n", tmp->count, tmp->str);
	       }

	       if(tmp->left){
		    tmp = tmp->left;
	       }
	       else{
		    tmp = parent->right;
	       }
	       traverse(tmp);
	       printf("raus\n");
	       return;

}



int main(int argc, char **argv)
{
     int i = 0;

     if(argc >= 1){
	  printf("Parameters:\n");
	  for(i = 0; i<argc; i++){
	       printf("argv[%d] == %s\n",i, argv[i]);
	  }

	  pnode root = NULL;
	  
	  for(i = 1; i<argc; i++){
	       root = insert(root, argv[i]);
	  }
	  
	  traverse(root);
     }
     
     
     return 0;
}

