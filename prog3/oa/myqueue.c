#include <stdio.h>
#include <stdlib.h>

struct queue 
{
     struct queue* next;
     int value;
};

typedef struct queue *lqueue;

extern lqueue enter(lqueue, int);
extern int is_empty(lqueue);
extern int front(lqueue);
extern lqueue leave(lqueue);

int is_empty(lqueue q)
{
     return (q == NULL);
}

int front(lqueue q)
{
     return q->value;
}

lqueue leave(lqueue q)
{
     lqueue qt = q;

     if(is_empty(q)){
	  return q;
     }

     q = q->next;
     free(qt);

     return q;
}

lqueue enter(lqueue q, int i)
{
     lqueue qt = q;
     lqueue tmp = (struct queue *)malloc(sizeof(struct queue));

     tmp->value = i;
     tmp->next = NULL;
     
     if(is_empty(q)) {
	  q = tmp;
     }
     else{
	  while(qt->next){
	       qt = qt->next;
	  }
	  qt->next = tmp;
     }
     return q;
}
