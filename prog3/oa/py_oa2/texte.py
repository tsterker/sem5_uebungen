#!/usr/bin/python
# -*- coding: utf-8 -*-
def konsonanten(s):
    konsonanten = "bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ"
    count = 0
    for c in s:
        if c in konsonanten:
            count += 1
    return count

def vokale(s):
    vd = {"a":0,"e":0,"i":0,"o":0,"u":0,"A":0,"E":0,"I":0,"O":0,"U":0}
    for c in s:
        if c in vd:
            vd[c] += 1
    return vd

def haeufigste_kleine_vokale(s):
    smalls = "aeiou"
    smallvocs = [x for x in vokale(s).items() if x[0] in smalls]
    smallvocs.sort(key=lambda x: x[1])  # by second item
    return smallvocs[::-1]

if __name__ == '__main__':
    fname = 'words'
    s = file(fname).read()
    print "Anzahl Konsonanten", konsonanten(s)
    print "Vokale", vokale(s)
    print u"haeufigste kleine Vokale", haeufigste_kleine_vokale(s)
