#! /usr/bin/python
# -*- coding: utf-8 -*-

class Ring(object):
    def __init__(self, *seq):
        self.ring = []
        if not seq:
            seq = []
        elif isinstance(seq[0], (list, tuple)):
            seq = seq[0]
        self.ring.extend(seq)

        self.current = 0

    def add(self, ele):
        if self.current == 0:
            self.ring.append(ele)
        else:
            self.ring.insert(self.current, ele)
            self.current = (self.current+1)%len(self)

    def add_sequence(self, seq):
        for ele in seq:
            self.add(ele)

    def get_elements(self):
        return self.ring[self.current:] + self.ring[:self.current]

    def get_current(self):
        return self.ring[self.current]

    def remove_current(self):
        self.ring.pop(self.current)
        if self.current == len(self.ring):
            self.current = 0

    def next(self):
        ret = self.get_current()
        self.current  = (self.current + 1)%len(self)
        return ret

    def is_empty(self):
        return len(self.ring) == 0

    def clear(self):
        self.ring = []
        self.current = 0

    def __str__(self):
        ret = ""
        i = self.current
        for ele in self.ring[:i]:
            #join with the fucking comma in the klammers
#        ret = ', '.join(self.ring[:i])
            ret = ''.join([ret, str(ele), ", "])
        ret = ''.join([ret[:-2], "; "])

        for ele in self.ring[i:]:
#        ret = ''.join([ret, ', '.join(self.ring[:i])])
            ret = ''.join([ret, str(ele), ", "])
        ret = ''.join([ret[:-2], "]"])
        ret = ''.join(["r[", ret])
        return ret

    def __repr__(self):
        return self.__str__()

    def __len__(self):
        return len(self.ring)

    def __contains__(self, other):
        return (other in self.ring)

    def __getitem__(self, index):
        i = (self.current+index)%len(self)
        return self.ring[i]

    def __iter__(self):
        class RingIter(object):
            def __init__(self, seq):
                self.ring = Ring(seq)
                self.length = len(self.ring)

            def next(self):
                if self.length == 0:
                    raise StopIteration
                return self.ring.next()
        return RingIter(self.get_elements())


def test_ring():
    r = Ring([1,2,3])
    for x in r:
        print x
        r.add("x")
        r.next()
        for y in r:
            print y
        print "----"

def test_init():
    r = Ring()
    print r
    r = Ring(1)
    print r
    r = Ring(1,2,3)
    print r
    r = Ring([1,2,3])
    print r


if __name__ == '__main__':
    test_ring()
