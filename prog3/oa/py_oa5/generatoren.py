import itertools as it

def gen(i):
    for x in range(i):
        yield x+1

def pendel(g):
    rev = []
    for x in g:
        rev.append(x)
        yield x
    while True:
        for x in reversed(rev[:-1]):
            yield x
        for x in rev[1:]:
            yield x

def im_kreis(g):
    for e in it.cycle(g):
        yield e

def von_vorne(g):
    tmp = []
    while True:
        tmp.append(g.next())
        for e in tmp:
            yield e

if __name__ == "__main__":
    from unittest import TestCase, TestSuite, TextTestRunner, makeSuite
    from itertools import islice

    class Test(TestCase):
        """ Unittest fuer generatoren
        (c) by Jan Staubach
        """

        def setUp(self):
            def infinite():
                while 1:
                    yield 1
                    yield 2
                    yield 3

            self.finite = (x for x in xrange(1, 4))
            self.infinite = infinite()

        def test_pendel_finite(self):
            to_test = islice(pendel(self.finite), 9)
            self.assertEqual(tuple(to_test), (1, 2, 3, 2, 1, 2, 3, 2, 1))

        def test_pendel_infinite(self):
            to_test = islice(pendel(self.infinite), 6)
            self.assertEqual(tuple(to_test), (1, 2, 3, 1, 2, 3))

        def test_im_kreis_finite(self):
            to_test = islice(im_kreis(self.finite), 4)
            self.assertEqual(tuple(to_test), (1, 2, 3, 1))

        def test_im_kreis_infinte(self):
            to_test = islice(im_kreis(self.infinite), 7)
            self.assertEqual(tuple(to_test), (1, 2, 3, 1, 2, 3, 1))

        def test_von_vorne_finite(self):
            to_test = islice(von_vorne(self.finite), 6)
            self.assertEqual(tuple(to_test), (1, 1, 2, 1, 2, 3))

            gen = von_vorne((x for x in range(0)))
            self.assertRaises(StopIteration, gen.next)

        def test_von_vorne_infinite(self):
            to_test = islice(von_vorne(self.infinite), 7)
            self.assertEqual(tuple(to_test), (1, 1, 2, 1, 2, 3, 1))

    suite = TestSuite()
    suite.addTest(makeSuite(Test))
    TextTestRunner(verbosity=4).run(suite)
