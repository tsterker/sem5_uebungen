#include <stdio.h>
#include <stdlib.h>
#include <string.h>



/* TODO::
   + falsche parameter abfangen? z.B. 10100x190
     (darf ich davon ausgehen, dass alle eingaben nur 01 sind?)
     (davon ausgehen, dass es immer ein richtiges ergebnis geben muss?)
     (davon ausgehen, dass keine sonderfälle abzufangen sind?)
   + eigene vergleicher schreiben oder bitwise benutzen?
   + strcmp erlaubt?

 */

void print_result(unsigned int);
unsigned int parse_operands(unsigned int, unsigned int, char *);
unsigned int bin_to_uint(char *);
unsigned int and(unsigned int, unsigned int);
unsigned int or(unsigned int, unsigned int);
unsigned int xor(unsigned int, unsigned int);
unsigned int lshift(unsigned int, unsigned int);
unsigned int rshift(unsigned int, unsigned int);

char *operands[5] = {"and","or","xor","lshift", "rshift"};
unsigned int (*functions[5])(unsigned int, unsigned int) = {&and, &or, &xor, &lshift, &rshift};
     

void print_result(unsigned int result)
{
     int i = 31;
     int first_one_occured = 0;

     for(; i >= 0; i--){
	  if(result & (1<<i)){
	       first_one_occured = 1;
	       putchar('1');
	  }
	  else if(first_one_occured || 0 == i)
	       putchar('0');
     }
     putchar('\n');
}



unsigned int bin_to_uint(char *str)
{
     int i = 0;
     unsigned int ui = 0;
     
     for(;str[i] != '\0';i++){
	  ui = ui<<1;
	  ui |= (str[i]-'0');
     }
     return ui;
}

unsigned int parse_operands(unsigned int p1, unsigned int p2,char *str)
{
/*     unsigned int (*op)(unsigned int, unsigned int) = NULL; */

     int i = 0;
     for (; i<5; i++){
	  if(0 == strcmp(operands[i], str)){
	       return (*functions[i])(p1, p2);
	  }
     }

     return 0;
}

unsigned int and(unsigned int p1, unsigned int p2)
{
     return p1 & p2;
}
unsigned int or(unsigned int p1, unsigned int p2)
{
     return p1 | p2;
}
unsigned int xor(unsigned int p1, unsigned int p2)
{
     return p1 ^ p2;
}
unsigned int lshift(unsigned int p1, unsigned int p2)
{
     return p1<<p2;
}
unsigned int rshift(unsigned int p1, unsigned int p2)
{
     return p1>>p2;
}

int main(int argc, char **argv)
{
     unsigned int result = 0;
     unsigned int p1 = 0;
     unsigned int p2 = 0;
     char *str;
     
     if(argc >= 4){
	  p1 = bin_to_uint(argv[1]);
	  p2 = bin_to_uint(argv[3]);
	  str = argv[2];
	  result = parse_operands(p1, p2, str);
	  print_result(result);
	  
     }
     else{
	  printf("Falsche Anzahl an Parametern!\nerwartet: 3\ntatsaechlich: %d\n", argc-1);
	  return -1;
     }
     

     return result;
}

     
