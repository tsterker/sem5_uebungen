
extern int my_strcasecmp(const char *s1, const char *s2);
extern const char *my_index(const char *s, int c);

int my_strcasecmp(const char *s1,const char *s2)
{
     int diff = 0;
     int n1, n2;

     /* while both strings have characters */
     while(*s1 != '\0' || *s2 != '\0'){
	  /* convert to lower case */
	  n1 = (n1 = *s1) < 'a' && *(s1) && *s1-'A' >= 0 ? n1+32 : n1;
	  n2 = (n2 = *s2) < 'a' && *(s2) && *s2-'A' >= 0 ? n2+32 : n2;

	  diff = n1-n2;

	  if(diff != 0){
	       return diff;
	  }
	  s1++;
	  s2++;
     }
     return diff;
}

const char *my_index(const char *s, int c)
{
     while(*s != '\0'){
	  if(*s == c){
	       return s;
	  }
	  s++;
     }
     return (char *)0; /* NULL pointer */
}
