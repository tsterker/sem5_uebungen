import sys

file_options = []

def heller(l, p):
    MAX = int(file_options[3])
    if p < -100 or p > 100:
        return l
    ret = []
    for v in l:
        v = int(v) + (p/100.0)*MAX
        v = int(v) if v <= 255 else 255
        ret.append(v)
    return ret

def gamma(l, g):
    MAX = int(file_options[3])
    gam = lambda g,v: MAX*(1.0*v/MAX)**g
    if g < 0 or g > 10:
        return l
    ret = []
    for v in l:

        v = gam(g,int(v))
        v = int(v) if v <= 255 else MAX
        ret.append(v)
    return ret

def binarisieren(l, s):
    MAX = int(file_options[3])
    if s < 0 or s > MAX:
        return l
    ret = []
    for v in l:
        v = MAX if int(v) >= s else 0
        ret.append(v)
    return ret


def list_fom_pgm(filename):
    f = open(filename, "r")
    not_empty = lambda line: (line != '\n' and line != '')
    l = list(line.strip().split() for line in f if not_empty(line) and line[0] != "#")

    for x in l[:3]:
        file_options.extend(x)
    l = l[3:]

    flat = []
    for x in l:
        flat.extend(x)
    return flat

def pgm_from_list(l, dest):
    f = open(dest,"w")
    i = 1
    #header
    f.write(file_options[0])
    f.write("\n")
    f.write(file_options[1])
    f.write(' ')
    f.write(file_options[2])
    f.write("\n")
    f.write(file_options[3])
    f.write("\n")
    f.write("\n")
    # pixels
    for x in l:
        f.write(str(x))
        f.write(" ")
        i += 1

options = {"heller":heller, "gamma":gamma, "binarisieren":binarisieren}

if __name__ == "__main__":
    if len(sys.argv) == 5:
        method = sys.argv[1]
        value = float(sys.argv[2])
        source = sys.argv[3]
        dest = sys.argv[4]
        l = list_fom_pgm(source)
        l = options[method](l,value)
        pgm_from_list(l, dest)
    else:
        print "Wrong Parameters:", sys.argv
        print "expected: ", sys.argv[0] , "[method] [value] [source-file] [target-file]"
