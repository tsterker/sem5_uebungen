#!/bin/bash
# (c) 2012 Alexander Baumgärtner

createPics(){
mkdir -p ausgabe
echo "Berechne Bild mit kanten.py c bilder/"$1".pgm ausgabe/"$1"_kanten_c.pgm"
python kanten.py  c bilder/$1.pgm ausgabe/$1_kanten_c.pgm
echo "Berechne Bild mit kanten.py python bilder/"$1".pgm ausgabe/"$1"_kanten_python.pgm"
python kanten.py python bilder/$1.pgm  ausgabe/$1_kanten_python.pgm

}

print_line(){

echo -e $2"\c"
sleep $1
echo -e $3"\c"
sleep $1
echo -e $4"\c"
sleep $1
echo -e $5"\c"
sleep $1
echo -e $6"\c"
sleep $1
echo -e $7"\c"
sleep $1
echo -e $8"\c"
sleep $1
echo -e $9"\c"
sleep $1
}


clear
echo ""
sleep 0.2
echo -e "             =\c"
print_line 0.01 = = = = = = = =
print_line 0.01 = = = = = = = =
print_line 0.01 = = = = = = = =
print_line 0.01 = = = = = = = =
print_line 0.01 = = = = = = = =
echo ""
sleep 0.075
echo -e "                  \c"
print_line 0.01 P r o g 3 "\x20" A b
print_line 0.01 g a b e "\x20" 10 "\x20" T
print_line 0.01 e s t - S k r i 
print_line 0.01 p t "\x20" "\x20" "\x20" "\x20" "\x20" "\x20"
echo ""
sleep 0.075
echo -e "             =\c"
print_line 0.01 = = = = = = = =
print_line 0.01 = = = = = = = =
print_line 0.01 = = = = = = = =
print_line 0.01 = = = = = = = =
print_line 0.01 = = = = = = = =
echo ""
echo -e "                  \c"
print_line 0.01 © "\x20" 2 0 1 2 "\x20" A 
print_line 0.01 l e x a n d e r
print_line 0.01 "\x20" B a u m g ä r 
print_line 0.01 t n e r "\x20" "\x20" "\x20"
echo ""
echo ""
sleep 0.25
echo "Die Abgabe wird gleich überprüft. Dazu müssen die Dateien kanten.py, cfilter.c und pyfilter.py im Ordner"
pwd
echo "vorhanden sein. Zum Testen werden mehrere Bilder im Ordner ausgabe erstellt und anschließend mit den Beispiellösungen im Ordner bspLsg verglichen. Das kann etwas dauern. Zum Starten ENTER drücken."
read
echo "Datei cfilter.so wird erstellt..."
rm cfilter.o -f
rm cfilter.so -f
rm pyfilter.pyc -f
echo "*** Beginn gcc Warnungen ***"
gcc -c -g -ansi -Wall -pedantic -Wextra -fPIC -shared cfilter.c -o cfilter.o 
gcc -g -ansi -Wall -pedantic -Wextra -fPIC -shared cfilter.o -o cfilter.so
echo "*** Ende gcc Warnungen ***"
echo ""
echo "Bilder werden erstellt..."
createPics lena
createPics 16x9
createPics lenaMitKommentaren
createPics 2x3
createPics 3x2
createPics line
createPics line2
echo ""
echo "Bilder werden getestet..."
python checkKanten.py
rm cfilter.o -f
rm cfilter.so -f
rm pyfilter.pyc -f
echo ""
echo "Ende der Tests. Die Bilder im Ordner ausgabe können gelöscht werden."
echo "Diese Nachricht zerstört sich mit ENTER selbst."
read
clear
exit
