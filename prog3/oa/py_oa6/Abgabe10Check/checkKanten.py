#!/usr/bin/python
#-*- coding: utf-8 -*-
# (c) 2012 Alexander Baumgärtner

checks = 0
correct = 0

def PGMequals(filename1, filename2):
    global checks
    checks += 1
    file1=file(filename1).read()
    file1=file1.split()
    file2=file(filename2).read()
    file2=file2.split()

    if file1==file2:
        print "OK: Bilder "+str(filename1)+" und "+str(filename2)+" sind gleich."
        global correct
        correct += 1
    else:
        print"FEHLER: Bilder "+str(filename1)+" und "+str(filename2)+" sind nicht gleich."

if __name__=='__main__':

    PGMequals("ausgabe/lena_kanten_c.pgm","bspLoesung/lena_kanten.pgm")
    PGMequals("ausgabe/lena_kanten_python.pgm","bspLoesung/lena_kanten.pgm")
    PGMequals("ausgabe/lenaMitKommentaren_kanten_c.pgm","bspLoesung/lenaMitKommentaren_kanten.pgm")
    PGMequals("ausgabe/lenaMitKommentaren_kanten_python.pgm","bspLoesung/lenaMitKommentaren_kanten.pgm")
    PGMequals("ausgabe/2x3_kanten_c.pgm","bspLoesung/2x3_kanten.pgm")
    PGMequals("ausgabe/2x3_kanten_python.pgm","bspLoesung/2x3_kanten.pgm")
    PGMequals("ausgabe/3x2_kanten_c.pgm","bspLoesung/3x2_kanten.pgm")
    PGMequals("ausgabe/3x2_kanten_python.pgm","bspLoesung/3x2_kanten.pgm")
    PGMequals("ausgabe/16x9_kanten_c.pgm","bspLoesung/16x9_kanten.pgm")
    PGMequals("ausgabe/16x9_kanten_python.pgm","bspLoesung/16x9_kanten.pgm")
    PGMequals("ausgabe/line_kanten_c.pgm","bspLoesung/line_kanten.pgm")
    PGMequals("ausgabe/line_kanten_python.pgm","bspLoesung/line_kanten.pgm")
    PGMequals("ausgabe/line2_kanten_c.pgm","bspLoesung/line2_kanten.pgm")
    PGMequals("ausgabe/line2_kanten_python.pgm","bspLoesung/line2_kanten.pgm")

    print ""
    print str(correct)+" von "+str(checks)+" Tests bestanden. Das sind "+str(correct*100/checks) +" Prozent."
