#include <stdio.h>
#include "cfile.h"

void test()
{
     printf("print in test()\n");
}

int printm(double** ptr, int ny, int nx)
{
     int i, j;
     for(i=0; i<ny; i++)
     {
          for(j=0; j<nx; j++)
          {

               printf("%.3f \t", *(*(ptr+i)+j));
          }
          printf("\n");
     }
     return 0;
}

int printp(int** ptr, int ny, int nx)
{
     int i, j;
     for(i=0; i<ny; i++)
     {
          for(j=0; j<nx; j++)
          {

               printf("%d \t", *(*(ptr+i)+j));
          }
          printf("\n");
     }
     return 0;
}


int main(int argc, char *argv)
{
     printf("print in main. Wird von python nicht geprintet!\n");
     return 0;
}
