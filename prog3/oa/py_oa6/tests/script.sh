#!/bin/bash

programm=${@:1:2}
echo "create shared libary of '$programm?'"
echo "<press ENTER>"
read dummy

if [ "$programm" = "" ];
then
echo "Kein Programmname eingegeben";
exit 1
fi

# remove old files
rfiles="lib$programm* $programm.o"
rm -f $rfiles

gcc -fPIC -DPIC -c $programm.c
gcc -shared -o lib$programm.so.1.0 -W-soname $programm.o
ln -s lib$programm.so.1.0 lib$programm.so
