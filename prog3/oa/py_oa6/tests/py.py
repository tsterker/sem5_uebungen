import ctypes as ct

bib = ct.CDLL("libcfile.so")
bib.test.restype = None
print "---"
print bib.test(None)

DOUBLE = ct.c_double
PDOUBLE = ct.POINTER(DOUBLE)
PPDOUBLE = ct.POINTER(PDOUBLE)
INT = ct.c_int

#matrix = ct.cdll.LoadLibrary('matrix.dll')
#print_matrix = getattr(matrix,'print')

# An array of doubles can be passed to a function that takes double*.
DBL5ARR = DOUBLE * 5
# An array of double* can be passed to your function as double**.
PDBL4ARR = PDOUBLE * 4

# Declare double* array.
ptr = PDBL4ARR()
for i in range(4):
    # fill out each pointer with an array of doubles.
    ptr[i] = DBL5ARR()
    for j in range(5):
        ptr[i][j] = i + j  # just to initialize the actual doubles.

bib.printm(ptr,4,5)
