
m1 = [1,1,0,
      1,1,0,
      0,0,1,]

m2 = [3,2,0,
      0,3,0,
      0,0,3,]

z = [0,0,0,
     0,0,0,
     0,0,0,]

# multiply two flat matrices, return result
def mmul(m1,m2):
    res = []
    c = 0

    for i in range(3):
        for j in range(3):
            s = sum([ t[0]*t[1] for t in zip(m1[i::3], m2[j::3]) ])
            res.append(s)
    return res

mmul(m1,m2)
