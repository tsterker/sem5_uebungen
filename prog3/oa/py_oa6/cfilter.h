#ifndef __CFILTER_H
#define __CFILTER_H

void test(int *a);
void neighbours_of(int **p2d, int width, int height, int iw, int ih, int *nbuffer);
int apply_matrix_get_sum(int *nbuffer, int *matrix);
int c_filter(int **xxx, int **ptr, int ny, int nx);



#endif /* __CFILTER_H */
