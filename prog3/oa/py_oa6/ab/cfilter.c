#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


int ga[9] = {1, 0, -1,
             2, 0, -2,
             1, 0, -1};

int gb[9] = { 1,   2,  1,
              0,   0,  0,
              -1,  -2, -1};

void test(int *a)
{
     a[0] = 1337;
     printf("print in test() - NEWW\n");
}


void neighbours_of(int **p2d, int width, int height, int iw, int ih, int *nbuffer)
{
     int i, j;
     for( i = -1; i <= 1; i++){
          for( j = -1; j <= 1; j++){

               if( i == 0 && j == 0 || (ih+i) < 0 || (ih+i) > height-1 || (iw+j) < 0 || (iw+j) > width-1){
                    *(nbuffer++) = 0;
               }
               else{
                    *(nbuffer++) = p2d[ih+i][iw+j];
/*
  printf("neighour: %d\n", *(nbuffer-1));
*/
               }
          }
     }
}

int apply_matrix_get_sum(int *nbuffer, int *matrix)
{
     int i, sum = 0;

     for (i = 0; i < 9; i++){
          sum += nbuffer[i]*matrix[i];
     }
     return abs(sum);
}


int c_filter(int **xxx, int **ptr, int ny, int nx)
{
     int i, j, k;
     int pixelc = ny*nx;
     int *nbuffer = malloc(9*sizeof(int));
     int *res = malloc(pixelc * sizeof(int*));
     int sum, max = 0;

     for(i=0; i<ny; i++)
     {
          for(j=0; j<nx; j++)
          {
               neighbours_of(ptr, nx, ny, j, i, nbuffer);
               for(k = 0; k < 9; k++){
/*                    printf("%d ", nbuffer[k]);*/
               }
               sum = apply_matrix_get_sum(nbuffer, ga) + apply_matrix_get_sum(nbuffer, gb);

               if (max < sum)
                    max = sum;
               res[i*nx + j] = sum;
/* printf("max == %d\n", max); */

          }
     }



     for(i = 0; i < pixelc; i++){
/*          printf("res == %d", res[i]); */
          xxx[i] = malloc(sizeof(res[i]));
          *(xxx[i]) = res[i];

          printf("%d ", *xxx[i]);
     }


     return max;
}


int main(int argc, char **argv)
{

     printf("print in main. Wird von python nicht geprintet!\n");
     return 0;
}
