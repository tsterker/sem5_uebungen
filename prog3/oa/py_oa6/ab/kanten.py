#! /usr/bin/python
# -*- coding: utf-8 -*-

#TODO
# + do() methode noch rübba
# + to_pgm() ööch rübbor
# + ALGORITHMUS NOCH NET GANZ RICHTIG??
# + max gray value anpassen??
# + abs ganz am ende oder zwischendurch?
# + properties für width, height

import sys
import os
import string
from pyfilter import python_filter


class VImage(object):
    def __init__(self, filename="lena.pgm"):

        hd = {0:"magic_num", 1:"width", 2:"height", 3:"max_grey"}
        is_comment = lambda line: line[0] == '#'
        values = []
        line_it = iter(open(filename, "r").readlines())

        for line in open(filename, "r").readlines():
            if is_comment(line):
                continue
            else:
                values.extend(line.strip().split())
#        values = [p for line in line_it for p in line.strip().split()  if not is_comment(line)]
        self.header = self.extract_header(values[:4])
        self.values = values[4:]
        self.pixels2d = self.make_2d_array(self.values)
        self.filename = filename


    def __str__(self):
        s = "[VImage] "+self.filename+"\n"+"header: "+str(self.header)
        return s

    def make_2d_array(self, values):
        pixels2d = []
        w, h = (int(self.header["width"]) , int(self.header["height"]))
        for i in range(h):
            x = i*w
            pixels2d.append([int(p) for p in values[x:x+w]])
        return pixels2d

    def make_2d_array_inplace(self, values):
        p2d = []
        w, h = (int(self.header["width"]) , int(self.header["height"]))
        for i in range(h):
            x = i*w
            p2d.append([int(p) for p in values[x:x+w]])
        self.pixels2d = p2d

    def extract_header(self, values):
        try:
            pgm_header = {}
            pgm_header["magic_num"] = values[0]
            pgm_header["width"], pgm_header["height"] = values[1],values[2]
            pgm_header["max_grey"] = values[3]
        except BaseException as e:
            print e
            print "Fehlerhafter Header:"+str(values)+"Programm wird beendet."
            exit(1)
        return pgm_header


    def to_pgm(self, filename="out.pgm"):
        f = open(filename, "w")
        # header
        f.write(self.header["magic_num"]+"\n");
        f.write(self.header["width"]+" "+self.header["height"]+"\n");

        cur_mx = int(self.header["max_grey"])
        mx = max(self.flat_list())
        mx = mx if mx > cur_mx else cur_mx
        print "MX == ", mx
        f.write(str(mx)+"\n");

        # pixel
        for i, p in enumerate(self.flat_list()):
            if i%20 == 0 and i != 0:
                f.write("\n")
            f.write(str(p)+" ")
        f.write("\n")
        f.close()



    def flat_list(self):
        l = []
        width, height = self.header["width"], self.header["height"]
        for h in range(int(height)):
            for w in range(int(width)):
                l.append(self.pixels2d[h][w])
        return l




if __name__ == '__main__':
#    print "Python Online-Abgabe 06"
#    print "-"*79

    try:
        impl, src, target = sys.argv[1:4]
    except ValueError:
        print "usage:"
        print sys.argv[0], "<source-file>", "<target-file>"
        exit(1)
    except IOError, e:
        print "Bitte gueltige <source-file> angeben"

    if impl == "python":
        img = VImage(src)
        img.pixels2d = python_filter(img.pixels2d, img.header, target)
        img.to_pgm(target)

    elif impl == "c":
        import ctypes as ct

#        bib = ct.CDLL("libcfilter.so")
        fname = os.path.join(os.getcwd(),'cfilter.so')
        bib = ct.cdll.LoadLibrary(fname)
        bib.test.restype = None
        bib.neighbours_of.restype = None;

        DOUBLE = ct.c_double
        PDOUBLE = ct.POINTER(DOUBLE)
        PPDOUBLE = ct.POINTER(PDOUBLE)
        CHAR = ct.c_char
        PCHAR = ct.POINTER(CHAR)
        PPCHAR = ct.POINTER(PCHAR)
        INT = ct.c_int
        PINT = ct.POINTER(INT)
        PPINT = ct.POINTER(PINT)

#matrix = ct.cdll.LoadLibrary('matrix.dll')
#print_matrix = getattr(matrix,'print')

        # Dimensions of array
        width = int(img.header["width"])
        height = int(img.header["height"])

        # An array of doubles can be passed to a function that takes double*.
        COLS = INT * width
        # An array of double* can be passed to your function as double**.
        ROWS = PINT * height

        # Declare double* array.
        ptr = ROWS()
        for i, row in enumerate(img.pixels2d):
            ptr[i] = COLS()
            for j, v in enumerate(row):
                ptr[i][j] = v  # just to initialize the actual doubles.

        elecount = width*height
        xxx = (PINT * elecount)()
#        bib.c_filter(hd, ptr,height, width)
        bib.c_filter(xxx, ptr,height, width)
        ls = []
        for i in range(elecount):
            ls.append(xxx[i][0])
        img.make_2d_array_inplace(ls)
        img.to_pgm(target)

#        bib.test(xxx)
