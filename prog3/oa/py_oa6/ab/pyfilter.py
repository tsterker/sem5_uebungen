#! /usr/bin/python

ga = [1,0,-1,
      2,0,-2,
      1,0,-1,]

gb = [ 1, 2, 1,
       0, 0, 0,
      -1,-2,-1]

def matsum(p, mat):
    s = 0
    for i, x in enumerate(p):
        s += x*mat[i]
    return s

# pixels2d and neighbours dict
def do(p2d, n):
    height,width = len(p2d), len(p2d[0])
    tmp1 = {}
    tmp2 = {}
    maxval = 0

    s1, s2 = 0,0
    for h in range(height):
        for w in range(width):
#           tmp1[(h,w)] = abs(matsum(n[(h,w)], ga))
#           tmp2[(h,w)] = abs(matsum(n[(h,w)], gb))
            val = abs(matsum(n[(h,w)], ga)) + abs(matsum(n[(h,w)], gb))
            p2d[h][w] = val
            maxval = val if val > maxval else maxval

#    for h in range(height):
#	for w in range(width):
#           p2d[h][w] = tmp1[(h,w)] + tmp2[(h,w)]
    return p2d

def dict_of_neighbours(p2d):
    height, width = len(p2d), len(p2d[0])
    d = {}
    for h in range(height):
        for w in range(width):
            vals = []
            for i in [-1,0,1]:
                for j in [-1,0,1]:
                    if i == j == 0 or (h+i) < 0 or (h+i) > height-1 or (w+j) < 0 or (w+j) > width-1:
                        vals.append(0)
                        continue
                    vals.append(p2d[h+i][w+j])
                    d[(h,w)] = vals
    return d


def python_filter(pixels2d, header, targ_file):
    d = dict_of_neighbours(pixels2d)
    pixels2d = do(pixels2d, d)
    return pixels2d

#    header["max_grey"] = str(max(flat_list(pixels2d, header["height"], header["width"])))
#    to_pgm(pixels2d, header, targ_file)
