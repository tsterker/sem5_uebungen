#!/bin/bash

#programm=${@:1:2}
programm="cfilter"

#echo "create shared libary of '$programm?'"
#echo "<press ENTER>"
#read dummy

if [ "$programm" = "" ];
then
echo "usage:"
echo "${@:0:1} <src-file>"
exit 1
fi

# remove old files
rfiles="$programm.so $programm.o"
rm -f $rfiles

gcc -fPIC -DPIC -c $programm.c
gcc -shared -o $programm.so -W-soname $programm.o
#ln -s lib$programm.so.1.0 lib$programm.so
