
/*
 * LIST
 * ==========================================
 * + adding elements (first, last, ith)
 *   -> malloc
 * + deleting elements (first, last, ith)
 *   -> free
 * + reverse in place
 */

pnode reverse_in_place(pnode root)
{
     pnode new_root = root;
     pnode next = root->next;
     
     if(!root->next)
	  return root;

     new_root = reverse_in_place(root->next);

     next->next = root;
     
     if(root->next->next == root)
	  root->next = NULL;
     return new_root;
}



/*
 * BINARY TREE
 * ==========================================
 * + adding elements (building tree)
 *   -> malloc
 * + traversing tree (smallest/biggest first)
 *   -> free
 */

struct tnode
{
     char *str;
     int count;
     struct tnode *left;
     struct tnode *right;
};
typedef struct tnode *ptnode;

/* ADD
 * + create (allocate) new node to insert
 *   -> for strings dont forget the +1 for terminatiing '\0'
 * + if root == NULL return new node as root
 * + traverse tree left/right if string to add is
 *   respectively less/greater than current node
 *   -> increase counter and return if string to add is already in tree
 *   -> free previously allocated new node
 * + if not returned yet
 *   --> tree leaf reached and new node to be added to it
 */
ptnode add(ptnode root, char *str)
{
     int diff;
     ptnode parent, tmp = root;
     ptnode new = (ptnode)malloc(sizeof(struct tnode));
     new->str = (char*)malloc(strlen(str)*sizeof(char)+1); /* +1 for terminating '\0' */
     strcpy(new->str, str);
     new->count = 1;
     new->left = NULL;
     new->right = NULL;

     if(!root)
	  return new;
     
     while(tmp){
	  parent = tmp;
	  diff = strcmp(new->str, tmp->str);
	  if(0 == diff){
	       tmp->count++;
	       free(new->str);
	       free(new);
	       return root;
	  }
	  else if(diff < 0){
	       tmp = tmp->left;
	  }
	  else{
	       tmp = tmp->right;
	  }
     }

     if(diff < 0){
	  parent->left = new;
     }
     else{
	  parent->right = new;
     }
     return root;
}

/* traverse binary search tree with left elemens smaller */
ptnode traverse(ptnode root)
{
     if(!root) return root;
     /* print here for smallest first */
     traverse(root->right);
     /* print here for biggest first */
     printf("tnode->str == %s\n", root->str);
     /* ... */
     return traverse(root->left);
}

/* angucken: baum umsortieren */
