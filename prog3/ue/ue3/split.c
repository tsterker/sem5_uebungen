#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int debug = 0;

struct split
{
     int num_parts;
     char **parts;
};
typedef struct split *psplit;

struct splits
{
     struct split **parts;
};
typedef struct splits *psplits;




int skip_delims(char *str, char *delim);
int count_parts(char *str, char *delim);
void clear(psplit ps);
psplit split(char *str, char *delim);



/*
 * Liefert anzahl zu ueberspringender chars zurueck.
 */
int skip_delims(char *str, char *delim)
{
     int skips = 0;
     int tmp_skips = 0;
     char *td = delim;

     while(*str == *td){
	  str++;
	  td++;
	  tmp_skips++;
	  if(!*td){
	       /* wrap deimiter around */
	       td = delim;
	       skips += tmp_skips;
	       tmp_skips = 0;
	  }
     }
     return skips;
}


int count_parts(char *str, char *delim)
{
     int parts = 0;
     int skips = 0;
     while(*str) {
	  if( (skips = skip_delims(str, delim)) ){
	       parts++;
	       str += skips;
	  }
	  else{
	       str++;
	  }
     }
/*
  es wird immer einses zu wenig gezaehlt....
*/
     parts++;
     return parts;
}


psplit split(char *str, char *delim)
{
     psplit ps = malloc(sizeof(struct split));
     int part_counter = 0;
     char *start = str;
     int skips = 0;
     int i = 0;

     ps->num_parts = count_parts(str, delim);
     ps->parts = (char **)malloc(ps->num_parts*sizeof(char*));

     while(*(str+i)) {
	  if( (skips = skip_delims((str+i), delim)) ){
	       *(str+i) = '\0';
	       ps->parts[part_counter] = malloc(sizeof(char)*100);
	       strcpy(*(ps->parts+part_counter),start);

	       part_counter++;
	       i += skips;
	       start = str+i;
	  }
	  else{
	       i++;
	  }
     }
     ps->parts[part_counter] = malloc(sizeof(char)*100);
     strcpy(*(ps->parts+part_counter),start);

     return ps;
}


void clear(psplit ps)
{
     int i = 0;
     while(i < ps->num_parts){
	  free(ps->parts[i]);
	  i++;
     }
     free(ps->parts);
     free(ps);
}

void print_split(psplit ps)
{
     int i;
     int np = ps->num_parts;
     printf("splitted in %d parts:\n[", np);
     for(i = 0; i < np; i++){
	  printf("%s", ps->parts[i]);
	  printf("%s", (i+1 == np)? "" : ",");
     }
     printf("]\n");
}


int main(int argc, char **argv)
{
     int offset = 1; /* index where words to split start */
     int delims = 1;
     int i = 0;
     int cwords = 0;

     if(argc > 2){
	  cwords = argc-offset-delims;

	  psplits results = (struct splits *)malloc(sizeof(struct splits));
	  results->parts = (psplit*)malloc(cwords * sizeof(psplit*));

	  for(i = 0; i < cwords; i++){
	       results->parts[i] = split(argv[i+offset+delims], argv[offset]);
	  }

	  for(i = 0; i < cwords; i++){
	       print_split(results->parts[i]);
	  }

	  for(i = 0; i < cwords; i++){
	       clear(results->parts[i]);
	  }
	  free(results->parts);
	  free(results);

     }
     else{
	  printf("usage: <delimiter> <word_to_split>[<word_to_split>]\n");
     }

     return 0;
}

