#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int debug = 0;

struct split
{
     int num_parts;
     char **parts;
};
typedef struct split *psplit;

struct splits
{
     struct split **parts;
};
typedef struct splits *psplits;


char *skip_delims(char *str, char *delim);
int count_parts(char *str, char *delim);
void clear(psplit ps);
psplit split(char *str, char *delim);


/*
 * If str starts with a delim, this function skips all delims in a row 
 * and returns a pointer that points to the position in str after the skipped delims.
 */
char * skip_delims(char *str, char *delim)
{
     char *td = delim;
     while(*str == *td){
	  str++;
	  td++;
	  if(!*td) td = delim;   /* wrap deimiter around */
     }
     return str;
}


int count_parts(char *str, char *delim)
{
     int parts = 0;
     char *tmp;

     while(*str) {
	  tmp = str;
	  str = skip_delims(str, delim);
	  if(tmp != str)
	       parts++;
	  else
	       str++;
     }
     parts++;
     return parts;
}


psplit split(char *str, char *delim)
{
     int part_counter = 0;
     char *start = str;
     char *tmp;

     psplit ps = malloc(sizeof(struct split));
     ps->num_parts = count_parts(str, delim);
     ps->parts = (char **)malloc(ps->num_parts*sizeof(char*));

     while(*str){
	  tmp = str;
	  str = skip_delims(str, delim);
	  if(tmp != str){
	       int diff = str-tmp;
	       *(str-diff) = '\0';
	       ps->parts[part_counter] = malloc(sizeof(char)*100);
	       strcpy(*(ps->parts+part_counter),start);
	       part_counter++;
	       start = str;
	  }
	  else{
	       str++;
	  }
     }
     ps->parts[part_counter] = malloc(sizeof(char)*100);
     strcpy(*(ps->parts+part_counter),start);

     return ps;
}


void clear(psplit ps)
{
     int i = 0;
     while(i < ps->num_parts){
	  free(ps->parts[i]);
	  i++;
     }
     free(ps->parts);
     free(ps);
}

void print_split(psplit ps)
{
     int i;
     int np = ps->num_parts;
     printf("splitted in %d parts:\n[", np);
     for(i = 0; i < np; i++){
	  printf("%s", ps->parts[i]);
	  printf("%s", (i+1 == np)? "" : ",");
     }
     printf("]\n");
}


int main(int argc, char **argv)
{
     int offset = 1; /* index where words to split start */
     int delims = 1;
     int i = 0;
     int cwords = 0;

     if(argc > 2){
	  cwords = argc-offset-delims;

	  psplits results = (struct splits *)malloc(sizeof(struct splits));
	  results->parts = (psplit*)malloc(cwords * sizeof(psplit*));

	  for(i = 0; i < cwords; i++){
	       results->parts[i] = split(argv[i+offset+delims], argv[offset]);
	  }

	  for(i = 0; i < cwords; i++){
	       print_split(results->parts[i]);
	  }

	  for(i = 0; i < cwords; i++){
	       clear(results->parts[i]);
	  }
	  free(results->parts);
	  free(results);

     }
     else{
	  printf("usage: <delimiter> <word_to_split>[<word_to_split>]\n");
     }

     return 0;
}

