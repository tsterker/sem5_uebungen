#! /usr/bin/python
# -*- coding: utf-8 -*-

import sys, random, string



def fromfile(filename="fortunes"):
    f = open(filename, "r")
    if not f:
        raise Exception(''.join(["No such file: ", filename]))
    fortunes = []
    tmp = ""
    for line in f.readlines():
        if line == "%\n":
            fortunes.append(tmp)
            tmp = ""
        else:
            tmp = "".join([tmp," ", line])

    return fortunes


def random_fortune():
    rand = int((random.random()*10*len(fortunes)))%len(fortunes)
    print "Random Forune no.", rand, ":"
    print fortunes[rand]


if __name__ == '__main__':
    fortunes = fromfile()

    print "sys.argv:",sys.argv
    if len(sys.argv) == 3 and sys.argv[1] == "-m":
        pattern =  sys.argv[2]
        for fortune in fortunes:
            if pattern in fortune:
                print fortune
                break

    else:
        random_fortune()
