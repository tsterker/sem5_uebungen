
# gesucht:
# die kleinste Zahl ist, die in zwei verschiedenen Arten als Summe von zwei
# Kubikzahlen dargestellt werden kann

qubes = [x**3 for x in range(1729)]
print "qubes:", qubes[:10]


print "zip(qubes, qubes)", zip(qubes, qubes)[:10]

r = [x*y for x,y in zip(qubes, qubes)]
print "r:", r[:10]
