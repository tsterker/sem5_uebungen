


even_qubic = [x**3 for x in range(11) if x**3%2 == 0]
print "even_qubic:",even_qubic

z = 10
all_divisors = [ x for x in range(2,z) if z%x == 0]
print "all_divisors:", all_divisors


primes =  [i for i in [x for x in range(2,20)] if len([y for y in range(2,i) if i%y == 0]) == 0]
print "primes", primes


primes2 =  [i for i in [x for x in range(10001,10100)] if len([y for y in range(2,i) if i%y == 0]) == 0]

print "primes2", primes2
