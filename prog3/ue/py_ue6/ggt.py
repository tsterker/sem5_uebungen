#!/usr/bin/python
# -*- coding: utf-8 -*-


def ints_fromfile(filename="ggts.dat"):
    f = open(filename, "r")
    ggts = []
    if not f:
        print "no such file:", filename
        return None

    print "reading from file:", filename

    for line in f.readlines():
        ggts.append(int(line))
    return ggts

def ggts_from_list(l):
    length = len(l)
    i = 0
    ggts = []
    while i < length-1:
        if i%2:
            ggts.append(ggt_rec2(l[i], l[i+1]))
        i += 1

    return ggts
            

def ggt_rec2(x,y):
    if x < y:
        x,y = (y,x)
    if not x%y:
        return y
    return ggt_rec2(x%y, y)
        
def ggt_rec(x,y):
    
    if x == y:
        return x
    elif x < y:
        x,y = y,x
    return ggt_rec(x-y,y)

lol = lambda x,y: (x%y,y) if x != y else ((y%x, x) if x < y else (x,x))



ggt_lambda = lambda x,y: 



# en größten gemeinsamen Teiler einer Liste von Zahlen berechnen
def ggTl(ggts):
    return reduce(ggt_rec2, ggts)

if __name__ == "__main__":
    ints = ints_fromfile()
    print "ggt_rec:"
    print ggt_rec(10,30)
    print ggt_rec(20,30)
    print ggt_rec(2,5)
    print ggt_rec(8,6)
    print ggt_rec(7,3)
    print "ggt_rec2:"
    print ggt_rec2(10,30)
    print ggt_rec2(20,30)
    print ggt_rec2(2,5)
    print ggt_rec2(8,6)
    print ggt_rec2(7,3)
    print "ggts_from_list(ggts)...."
    ggts_list = ggts_from_list(ints)
    print "ggTl - biggest ggT:"
    print ggTl(ints)
    print "ggTl([10,8,20,75]):", ggTl([10,8,20,75])
    print "ggTl([12,9,21,3]):", ggTl([12,9,21,3])

















