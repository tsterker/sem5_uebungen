#!/usr/bin/python
# -*- coding: utf-8 -*-

# for to while:
##################################################
l = [1,2,3]
print "For loop:"
for e in l:
    print e

print "While loop:"
i = len(l)-1
while i:
    print l[len(l)-i]
    i -= 1

# while to for:
##################################################
d = {1:"eins", 2:"zwei", 3:"drei"}
l = d.keys()
i = 0
print "While loop:"
while i < len(l):
    s = l[i]
    print s, d[s]
    i = i+1

print "For loop:"
for key,value in d.iteritems():
    print key, value


