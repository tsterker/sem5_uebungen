
class Set(object):
    def __init__(self, seq=[]):
        self.elems = seq

    def add(self, elem):
        if elem not in self.elems:
            self.elems.append(elem)

    def union_update(self, seq):
        for elem in seq:
            self.add(elem)

    def union(self, seq):
        ret = Set()
        ret.union_update(self.elems)
        ret.union_update(seq)
        return ret

    def remove(self, elem):
        if elem in self.elems:
            self.elems.remove(elem)

    def difference_update(self, seq):
        for elem in seq:
            if elem in self.elems:
                self.elems.remove(elem)

    def difference(self, seq):
        ret = Set()
        ret.union_update(self.elems)
        ret.difference_update(seq)
        return ret

    def clear(self):
        self.elems = []

    def size(self):
        return len(self.elems)

    def __len__(self):
        return self.size()

    def __contains__(self, key):
        return key in self.elems

    def __getitem__(self, index):
        return self.elems[index]

    def __eq__(self, other):
        if self.size() != other.size():
            return False
        for o in other:
            if o not in self.elems:
                return False
        return True

    def __neq__(self, other):
        return not (self == other)

    def __str__(self):
        return str(self.elems)

    def __add__(self, other):
        ret = Set()
        ret.union_update(self.elems)
        for elem in other:
            ret.add(elem)
        return ret

    def __radd__(self, other):
        return self + other

    def __sub__(self, other):
        ret = Set()
        ret.union_update(self.elems)
        for elem in other:
            ret.remove(elem)
        return ret

    def __rsub__(self, other):
        return self - other

class OrderedSet(Set):
    def __init__(self):
        self.current_index = None

    def __iter__(self):
        return sorted(self)

    def next(self):
        if self.current_index == self.size()-1:
            raise StopIteration
        else:
            self.current_index += 1
            return self.current_index - 1


if __name__ == '__main__':
    pass
