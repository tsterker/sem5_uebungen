#! /usr/bin/python
# -*- coding: utf-8 -*-

import sys

curr_lang = "en"

lang_dict = {
    "en": "The file {filename}:\n    lines: {lines}\n    words: {words}\n    chars: {chars}\n",
    "de": "Die Datei {filename}:\n    Zeilen: {lines}\n    Wörter: {words}\n    Buchstaben: {chars}\n",
    }

def lines(filename):
    return len(file(filename).readlines())

def chars(filename):
    return len(file(filename).read())

def words(filename):
    return len(file(filename).read().split())

def wc(filename):
    return lines(filename),words(filename),chars(filename)

def wc_show(filename):
    txt = lang_dict[curr_lang].replace("{filename}", filename)
    txt = txt.replace("{lines}", str(lines(filename)))
    txt = txt.replace("{words}", str(words(filename)))
    txt = txt.replace("{chars}", str(chars(filename)))
    print txt

def set_lang(lang):
    global curr_lang
    if lang in lang_dict:
        curr_lang = lang

if __name__ == '__main__':
    set_lang("de")
    wc_show("lol")
