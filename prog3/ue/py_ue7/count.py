

def count_words(filename):
    f = open(filename, "r")
    ret = {}
    words = f.read().split()
    for word in words:
	# get returns 0 as default, if key doesnt exist
	ret[word] = ret.get(word, 0) + 1
    return ret

def count_chars(filename):
    f = open(filename, "r")
    ret = {}
    chars = f.read()
    for char in chars:
	# get returns 0 as default, if key doesnt exist
	ret[char] = ret.get(char, 0) + 1
    return ret


if __name__ == '__main__':
    print count_chars("lol")
    print count_words("lol")
