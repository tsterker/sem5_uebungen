from itertools import islice


def prim(i):
    for x in range(2,i):
        if i%x == 0:
            return False
    return True

def primgen():
    i = 2
    while True:
        while not prim(i):
            i += 1
        yield i
        i += 1


def primpairgen():
    prev = 2
    for p in primgen():
        if p-prev == 2:
            yield (prev, p)
        prev = p


def test():
    i = 0
    print "primgen():"
    print "##################################################"
    print "for with break:"
    print "---------------"
    for p in primgen():
        if i > 10:
            break
        i += 1
        print p
    print "itertools.islice:"
    print "-----------------"
    for p in islice(primgen(), 0,10):
        print p

    print "generator.next() manually:"
    print "--------------------------"
    f = primgen()
    for j in range(10):
        print f.next()

    print "generator-comprehension:"
    print "------------------------"
    for p in (x for x in range(10)):
        print p

    print ""
    print "primpairgen():"

    i = 0
    for p in primpairgen():
        if i > 10:
            break
        i += 1
        print p
