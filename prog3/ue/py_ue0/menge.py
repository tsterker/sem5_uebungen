#! /usr/bin/python

class Set(object):
    ''' Implementierung eines Sets
    interne Representation mit einem dict'''

    def __init__(self, e=None):
        self.__data = dict([(x,x) for x in e]) if e else dict()

    def add(self, e):
        self.__data[e] = e

    def union_update(self, seq):
        for e in seq:
            self.add(e)

    def union(self, seq):
        ret = Set(list(self.__data))
        ret.union_update(seq)
        return ret

    def remove(self, e):
        if self.__data.get(e):
            del self.__data[e]

    def difference_update(self, seq):
        for e in seq:
            self.remove(e)

    def difference(self, seq):
        ret = Set(list(self.__data))
        ret.difference_update(seq)
        return ret


    def clear(self):
        self.__data = dict()

    def size(self):
        return len(self.__data)

    def __len__(self):
        return len(self.__data)

    def __iter__(self):
        class SetIter(object):
            def __init__(self, seq):
                self.__data = seq
                self.current = 0
                self.length = len(self.__data)

            def next(self):
                if self.current >= self.length:
                    raise StopIteration
                self.current = self.current + 1
                return self.__data[self.current-1]

        return SetIter(list(self.__data))

    def __contains__(self, e):
        return e in self.__data

    def __eq__(self, other):
        return self.__data == other.__data

    def __neq__(self, other):
        return not self == other

    def __add__(self, other):
        ret = Set(other)
        ret.union_update(self.__data)
        return ret

    def __radd__(self, other):
        return self + other

    def __sub__(self, other):
        ret = Set(other)
        ret.difference_update(self.__data)
        return ret

    def __rsub__(self, other):
        return self - other

    def __str__(self):
        return ''.join(["mySet(", str(list(self.__data)), ")"])

    def __repr__(self):
        return self.__str__()

class OrderedSet(Set):

    def __iter__(self):
        class OrderedSetIter(object):
            def __init__(self, seq):
                self.__data = sorted(seq)
                self.current = 0
                self.length = len(seq)

            def next(self):
                if self.current >= self.length:
                    raise StopIteration
                self.current = self.current + 1
                return self.__data[self.current-1]
        return OrderedSetIter(list(self.__data))


if __name__ == '__main__':
    print "modul menge"
    print "-------------"
    s = OrderedSet([2,1,3,2,4])
    os = OrderedSet([2,1,3,2,4])
    print "s",s
    print "os",os
