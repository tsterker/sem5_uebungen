#! /usr/bin/python

import sys
from collections import defaultdict

def count_words(words):
    wd = dict()
    max_count = 0
    for w in words:
        wd[w] = wd.get(w, 0) + 1
        max_count = wd[w] if max_count < wd[w] else max_count

    for k,v in wd.items():
        if v >= max_count:
            print v,":",k

# using default dict
def count_words_dd(words):
    wd = defaultdict(int) # default dict with int values
    max_count = 0
    for w in words:
        wd[w] +=  1 # default value 0 if key doesnt exist
        max_count = wd[w] if max_count < wd[w] else max_count

    for k,v in wd.items():
        if v >= max_count:
            print v,":",k



if __name__ == '__main__':
    print "Aufgabe 1 - Onlinetest 06/07"
    print "----------------------------"
    print "count_words() using dict:"
    count_words(sys.argv[1:])
    print "count_words() using defaultdict:"
    count_words_dd(sys.argv[1:])
