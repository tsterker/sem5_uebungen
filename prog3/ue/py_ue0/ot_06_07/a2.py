#! /usr/bin/python

import string
import sys

UPPER = string.uppercase
LOWER = string.lowercase

def comp(word):
    res = [] # store result as list for easier manipulation
    occur = 0 # count occurences in a row
    prev = "" # remember previous char

    for cur in word:
        cur = cur.lower() # ignore case
        if cur == prev and cur in UPPER+LOWER:
            occur += 1
            if occur == 1:
                res += [2,] # is set at second orrucence
            else:
                res[-1] += 1
        else:
            occur = 0
            res += [cur,]
        prev = cur
    return ''.join(str(c) for c in res) # list to string; generator expression


if __name__ == '__main__':
    print "Aufgabe 2 - Onlinetest 06/07"
    print "----------------------------"
    for word in sys.argv[1:]:
        print comp(word)
