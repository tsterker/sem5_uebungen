#! /usr/bin/python

import sys


def check_palindrom(word):
    s = ""
    if word == word[::-1]:
        s = ''.join(["*",word,"*"])
    else:
        s = ''.join([word,word[::-1]])
    print s.lower()

if __name__ == '__main__':
    print "Palindrom checker/generator"
    print "-"*79

    for word in sys.argv[1:]:
        check_palindrom(word)
    if len(sys.argv) < 2:
        print "usage:"
        print sys.argv[0],"<word> [<word>*]"
