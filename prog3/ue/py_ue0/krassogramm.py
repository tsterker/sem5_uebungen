#! /usr/bin/python


import sys
import string as s

letters = s.lowercase + s.uppercase
vocals = "aeiouAEIOU"


# DEFECT
def make_krass(word):
    ret = []
    occur = 0
    prev = ""

    for c in word:
        if c in vocals:
            if c == prev:
                occur += 1
                if occur == 3:
                    ret += [3,]
                elif occur > 3:
                    ret[-1] = 1
            else:
                ret += [c*occur,]
                occur = 0
        else:
                occur = 0
                ret += [c,]

        prev = c

    return ''.join(str(c) for c in ret)

if __name__ == '__main__':
    print "Krassogramm"
    print "-"*79

    for word in sys.argv[1:]:
        print make_krass(word)
