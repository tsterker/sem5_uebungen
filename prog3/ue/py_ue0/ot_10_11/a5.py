#! /usr/bin/python


class Klasse(object):
    count = 0
    def __init__(self, zahl1=None):
        Klasse.count += 1
        self.zahl1 = zahl1

    def __del__(self):
        print "deleting..."
        if Klasse:
            Klasse.count -= 1

    def add(self, para1):
        ret = 0
        if not self.zahl1:
            print "no zahl1 defined ==> para1*2"
            self.zahl1 = para1
        else:
            print "zahl1 defined ==> zahl1+para1"

        return self.zahl1 + para1

    @staticmethod
    def wieviel():
        return Klasse.count



class Erbe(Klasse):
    def __init__(self, zahl1=None, zahl2=None):
        super(Erbe, self).__init__(zahl1)
        self.zahl2 = zahl2
        print "self.zahl1:", self.zahl1
        print "self.zahl2:", self.zahl2


if __name__ == '__main__':
    print "Aufgab 5 - Python Onlinetest 10/11"
    print "----------------------------------"
    print "create 4 instances of Klasse"
    klassen = [Klasse(i) for i in range(2)]
    klassen.extend([Klasse() for i in range(2)])
    print "Wieviele: ", Klasse.wieviel()
    print "--------- Delete 2 Instances----------"
    del klassen[0]
    del klassen[-1]
    print "Wieviele: ", Klasse.wieviel()
    print "--------- execute add(1) on remaining Instances----------"
    for k in klassen:
        print k.add(1)

    print "--------------- ERBEN ------------------"
    print "create  6 instances of Erbe"
    erben = [Erbe(i,i) for i in range(2)]
    erben.extend([Erbe(i) for i in range(2)])
    erben.extend([Erbe(i) for i in range(2)])
    print "Wieviele: ", Klasse.wieviel()
    print "--------- Delete 3 Instances----------"
    for i in range(2):
        del erben[2]
        del erben[0]
        del erben[-1]
    print "Wieviele: ", Klasse.wieviel()
    print "--------- execute add(1) on remaining Instances----------"
    for k in erben:
        print k.add(1)
