#!/usr/bin/python
# -*- coding: utf-8 -*-

from a5 import Klasse, Erbe # die eigene zu testende Klassen
import unittest

class TestInheritance(unittest.TestCase):
    "Testen der Vererbungs-implementierung"

    def setUp(self):
        "vor jedem Test, Beispielklassen instanziieren"
        pass

    def test_wieviele(self):
        "Hilfsroutine, prüft anzahl der Instanzen"
        w = Klasse.wieviel()
        count = 0

        bases = [Klasse(i) for i in range(10)]
        count += 10
        children = [Erbe(i) for i in range(10)]
        count += 10
        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> w", w
        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> count", count
        self.assert_(count == w)

        #delete 10 instances
        for i in range(5):
            del bases[0]
            del children[0]
            count -= 2
        self.assert_(count == w)

        #delete rest
        del bases
        del children
        count = 0
        self.assert_(count == w)

    def test_add(self):
        "Teste add methode"

        self.bases = []
        self.children = []
        self.bases.append(Klasse(1))
        self.bases.append(Klasse())

        self.children.append(Erbe(1, 2))
        self.children.append(Erbe(1))
        self.children.append(Erbe())


        self.assertEqual(self.bases[0].add(3), 4)
        self.assertEqual(self.bases[1].add(3), 6)

        self.assertEqual(self.children[0].add(3), 4)
        self.assertEqual(self.children[1].add(3), 4)
        self.assertEqual(self.children[2].add(3), 6)

        # clean up
        del bases
        del children



if __name__ == '__main__':
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestInheritance))
    unittest.TextTestRunner(verbosity=2).run(suite)
