#! /usr/bin/python

from string import strip



def process(lines, split=":", key=0, del_key=False):
    ret = dict()
    for l in lines:
        parts = l.split(split)
        try:
            val = parts[:]
            if del_key:
                val.remove(parts[key])
            ret[parts[key]] = val
        except:
            print "ommiting line:", l

    return ret

if __name__ == '__main__':
    print "Aufgab 1 - Python Onlinetest 10/11"
    lines = map(strip,open("text", 'r').readlines())
    d = process(lines, key=1, del_key=True)
    print "------ processed ---------"
    print d
