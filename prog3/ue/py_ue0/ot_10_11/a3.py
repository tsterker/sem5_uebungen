#! /usr/bin/python

import itertools as it

def vielfach(g1, n):
    valid = n > 0
    while valid:
        current = g1.next()
        for i in range(n):
            yield current

def nmischen(g1, n, g2):
    valid = n > 0
    gviel = vielfach(g1, n)
    while valid:
        c = 0
        while c < n:
            yield gviel.next()
            c += 1
        yield g2.next()

def getestet(g1, test):
    gen = it.ifilter(test, g1)
    while True:
        yield gen.next()


def test_gen(gen):
    c = 0
    while c < 10:
        print gen.next()
        c += 1

if __name__ == '__main__':
    print "Aufgab 3 - Python Onlinetest 10/11"

    gen = (i for i in it.count())
    print "vielfach(gen, 2)"
    test_gen(vielfach(gen, 2))

    gen = (i for i in it.count())
    gen2 = ("x" for i in it.count())
    print "nmischen(gen, 2, gen2)"
    test_gen(nmischen(gen, 2, gen2))

    gen = (i for i in it.count())
    test = lambda x: (x > 3)
    print "getestet(gen, test)"
    test_gen(getestet(gen, test))
