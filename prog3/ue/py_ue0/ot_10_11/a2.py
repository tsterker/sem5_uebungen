#! /usr/bin/python
import string

vocs = "aeiouAEIOU"

def rumdreh(zk):
    return zk[::-1]

def vokale(zk):
    v = ""
    rest = ""
    for c in zk:
        if c in vocs:
            v += c
        else:
            rest += c
    return (v, rest)

def ersetze(zk, dic):
    for key in dic.keys():
        zk = zk.replace("{"+key+"}", dic[key])
    return zk

if __name__ == '__main__':
    print "Aufgabe 1 - Python Onlinetest 10/11"

    print "rumdreh('Baum'):"
    print rumdreh("Baum")

    print "vokale('Baum')"
    print vokale("Baum")

    print "ersetze('Das ist ein {adjektiv} {nomen}', {'adjektiv' : 'toller', 'nomen' : 'text'} )"
    print ersetze('Das ist ein {adjektiv} {nomen}', {"adjektiv" : "toller", "nomen" : "text"} )
