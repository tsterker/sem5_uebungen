#! /usr/bin/python


def add1(x):
    return x+1

def mul2(x):
    return x*2

def summe(*p):
    return sum(p)


def hintereinander(*funcs):
    def fn(param):
        for f in reversed(funcs):
            param = f(param)
        return param
    return fn

def partiell(func, *args):
    def fn(*params):
        return func(*(args+params))
    return fn

if __name__ == '__main__':
    print "Aufgab 4 - Python Onlinetest 10/11"
    print "-----------------------------------"
    print "fn = hintereinander(add1, mul2):"
    fn = hintereinander(add1, mul2)
    print "fn(3) == ", fn(3)
    print "-----------------------------------"
    print "fn = hintereinander(mul2, add1):"
    fn = hintereinander(mul2, add1)
    print "fn(3) == ", fn(3)
    print "-----------------------------------"
    print "fn = pariell(summe, 1,2,3):"
    fn = partiell(summe, 1,2,3)
    print "fn(3) == ", fn(4,5,6)
