def fn(function):
    def wrapper(*args, **kwargs):
        print "-"*79
        info = "| Arguments:"+str(args)+str(kwargs)
        offset = 77-len(info)
        print info, offset*" ", "|"
        print "-"*79
        return function(*args, **kwargs)
    return wrapper

i = 0

@fn
def write(text):
    i = 0
    print text

@fn
def write2(text, number, char):
    print "text:", text
    print "number:", number
    print "char:", char


if __name__ == '__main__':
    write("halloo")
    write2("hallo", 17, 'x')
