#! /usr/bin/python

import itertools as it

def gen(i=1, r=10):
    return (x*i for x in range(r))

def vielfach(g, n):
    while True:
        tmp = g.next()
        for i in range(n):
            yield tmp

def nmischen(g1, n ,g2):
    while True:
        tmp = g1.next()
        for i in range(n):
            yield tmp
        yield g2.next()

def getestet(g, test):
    while True:
        tmp = g.next()
        if test(tmp):
            yield tmp

def test_gen(gen):
    for x in gen:
        print x

if __name__ ==  '__main__':
    print "Aufgabe 3 - Generatoren"
    print "-"*79

    test = lambda x: (2 < x < 8)

    viel = vielfach(gen(), 2)
    misch = nmischen(gen(), 2, gen("x"))
    tst = getestet(gen(), test)

    test_gen(viel)
    print "-"*10
    test_gen(misch)
    print "-"*10
    test_gen(tst)
