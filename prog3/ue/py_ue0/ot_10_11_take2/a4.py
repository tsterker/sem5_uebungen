#! /usr/bin/python

def add1(x):
    return x+1

def mul2(x):
    return x*2

def summe(*p):
    return sum(p)

def hintereinander(*funcs):
    def fn(x):
        for f in reversed(funcs):
            x = f(x)
        return x
    return fn

def partiell(func, *args):
    def fn(*rest):
        return func(*(args+rest))
    return fn

if __name__ ==  '__main__':
    print "Aufgabe 4 - Member Functions"
    print "-"*79

    g = hintereinander(add1, mul2)
    print "g = hintereinander(add1, mul2)"
    print "g(3):", g(3)
    g = hintereinander(mul2, add1)
    print "g = hintereinander(mul2, add1):"
    print "g(3):", g(3)

    g = partiell(summe, 1,2,3)
    print "g = partiell(summe, 1,2,3)"
    print "g(4,5,6) = ", g(4,5,6)
