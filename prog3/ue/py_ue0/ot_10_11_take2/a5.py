#! /usr/bin/python


class Klasse(object):
    count = 0
    def __init__(self, zahl1=None):
        self.zahl1 = zahl1
        Klasse.count += 1

    def add(self, para1):
        return self.zahl1+para1 if self.zahl1 else para1*2

    @staticmethod
    def wieviel():
        return Klasse.count


class Erbe(Klasse):
    def __init__(self, zahl1=None, zahl2=None):
        Klasse.__init__(self, zahl1)
        self.zahl2 = zahl2

    def mul(self, para2):
        return self.zahl2*para2 if self.zahl2 else para2*3

    def __mul__(self, other):
        return self.mul(other)

    __rmul__ = __mul__



if __name__ ==  '__main__':
    print "Aufgabe 5 - Vererbung"
    print "-"*79

    base = Klasse(10)
    child = Erbe(500, 500)

    print "---------base.add(1)"
    print base.add(1)
    print "---------child.add(1)"
    print child.add(1)
    print "---------child.mul(2)"
    print child.mul(2)
    print "---------child * 2"
    print child * 2
    print "---------2 * child"
    print 2 * child
