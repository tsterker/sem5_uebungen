#! /usr/bin/python

import sys

def process(lines, split=":", key=0, del_key=False):
    ret = dict()
    for l in lines:
        l = l.strip().split(split)
        ret[l[key]] = l if not del_key else l[:key]+l[key+1:]

    return ret


if __name__ ==  '__main__':
    print "Aufgabe 1 - process()"
    print "-"*79

    try:
        lines = open(sys.argv[1]).readlines()
        print process(lines)
        print process(lines, key=1)
        print process(lines, key=1, del_key=True)

    except IOError, e:
        print "IOError:", e
        print "Bitte gueltigen dateinamen angeben"
    except IndexError, e:
        print "Benutzung:"
        print sys.argv[0],"<dateiname>"
