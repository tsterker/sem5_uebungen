#! /usr/bin/python

import sys

VOCALS = "aeiouAEIOU"

def rumdreh(w):
    return w[::-1]

def vokale(w):
    voc = []
    rest = []
    for c in w:
        if c in VOCALS:
            voc.append(c)
        else:
            rest.append(c)
    return (voc, rest)

def ersetze(w, dic):
    for k,v in dic.iteritems():
        w = w.replace("{"+k+"}", v)
    return w

if __name__ ==  '__main__':
    print "Aufgabe 2 - String Operationen"
    print "-"*79


    for word in sys.argv[1:]:
        print rumdreh(word)
        print vokale(word)
        print ersetze(word, {"a":"alpha", "b":"beta"})
        print "-"*10


    if len(sys.argv) < 2:
        print "Benutzung:"
        print sys.argv[0],"<wort> [<wort>*]"
