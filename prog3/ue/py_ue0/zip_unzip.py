#! /usr/bin/python

def myzip(*a):
    l = min([len(x) for x in a])
    return [ tuple([x[i] for x in a]) for i in range(l) ]

def myunzip(a):
    return myzip(*a)

if __name__ == '__main__':
    print "Zip + Unzip"
    print "----------------"
    t = [(1,2), (3,4), (5,6)]
    a = [1,3,5]
    b = [2,4,6]
    print "t:", t;     print "a:", a;     print "b:", b
    print "zip(a,b): ",zip(a,b)
    z = myzip(a,b)
    print "z = myzip(a,b): ", z
    print "------------------------------------------------"
    print "myunzip(z): ",myunzip(z)
