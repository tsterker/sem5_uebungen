#! /usr/bin/python

import sys


en = str(
    "The file %s has got:\n"
    "characters:\t%d\n"
    "words:\t%d\n"
    "lines:\t%d\n"
    )

de = str(
    "die Datei %s hat:\n"
    "Buchstaben:\t%d\n"
    "Woerter:\t%d\n"
    "Zeilen:\t%d\n"
    )

lang =  {"de":de, "en":en}


def chars(f):
    return len(open(f).read())

def words(f):
    return len(open(f).read().split())

def lines(f):
    return len(open(f).readlines())

def wc_show(f, lang="de"):
    lang
    print "die Datei",f,"hat"
    print "chars:",chars(f)
    print "words:",words(f)
    print "lines:",lines(f)

if __name__ == '__main__':
    print "Uebung 7, Aufgabe 1 - Wordcount"
    print "-"*79

    try:
        filename = sys.argv[1]
        f = open(filename)
        wc_show(filename)

    except IOError, e:
        print "usage:"
        print sys.argv[0],"<filename>"

    except IndexError, e:
        print "Bitte guelitigen Dateinamen angeben."
