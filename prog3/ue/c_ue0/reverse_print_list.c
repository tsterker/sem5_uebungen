#include <stdio.h>
#include <stdlib.h>
#include <string.h>



struct node
{
     char *str;
     struct node *next;
};

typedef struct node *pnode;



pnode add(pnode head, char *str)
{
     pnode tmp = head;
     pnode new = (pnode)malloc(sizeof(struct node));
     new->str = (char*)malloc(strlen(str)*sizeof(char)+1);
     strcpy(new->str, str);
     new->next = NULL;

     if(!head){
	  head = new;
  return head;
     }

     while(tmp->next){
	  tmp = tmp->next;
     }
     
     tmp->next = new;
     return head;
}



void clear(pnode head)
{
     pnode tmp;
     while(head){
	  tmp = head->next;
	  free(head->str);
	  free(head);
	  head = tmp;
     }
}


void print(pnode head)
{
     
     while(head->next){
	  printf("%s --> ", head->str);
	  head = head->next;
     }
     printf("%s", head->str);
     printf("\n");
}

pnode print_reverse(pnode head)
{
     if(!head)
	  return head;
     
     print_reverse(head->next);
     
     printf("<-- %s", head->str);

}

pnode rip(pnode head)
{
     pnode tmp, tmp2;
     tmp = NULL;
     tmp2 = head->next;
     
     while(tmp2){
	  tmp2 = head->next;
	  head->next = tmp;
	  tmp = head;
	  if(tmp2)
	       head = tmp2;
     }
     return head;
}


pnode reverse_in_place(pnode head)
{
     pnode next = head->next;
     pnode new_head = head;

/* head->next is saved by pnode 'next'
 * and therefore still available after recursion
 * and  only the very first list element wont 
 * have a next element assigned to it.
 * (the others next->next = head)
 */
     head->next = NULL; 
     if(next)
	  new_head = reverse_in_place(next);
     else
	  return new_head;
     /* the next of the current now points back to this */
     next->next = head;
     return new_head;
}



int main(int argc, char **argv)
{
     int i;
     pnode head = NULL;
     
     if(argc >= 2){
	  
	  for(i = 1; i< argc; i++){
	       head = add(head, argv[i]);
	  }
	  printf("---------print----------\n");
	  print(head);
	  printf("\n");
	  printf("---------print_reverse----------\n");
	  print_reverse(head);
	  printf("\n");
	  printf("---------rip (reverse in place)---------\n");
	  head = rip(head);
	  printf("\n");
	  printf("---------print----------\n");
	  print(head);
	  printf("\n");
	  printf("---------print_reverse----------\n");
	  print_reverse(head);
	  printf("\n");

	  printf("---------reverse_in_place(recursive)----------\n");
	  head = reverse_in_place(head);

	  printf("---------print----------\n");
	  print(head);
	  printf("\n");
	  

	  clear(head);
	  
     }
     else{
	  

     }
     
     
     return 0;
}


