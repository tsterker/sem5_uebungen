#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>




int hasindex(int i, int is[], int iscount)
{
     int x = 0;
     while(x < iscount){
	  if(*(is+x) == i)
	       return 1;
	  x++;
     }
     return 0;
}



char *delindex(char *str, int is[], int iscount)
{
     int len = strlen(str);
     char *tmp;
     int  i = 0;
     int deleted = 0;
     
     while(i+deleted < len){
	  
	  if(hasindex(i+deleted, is, iscount))
	  {
	       printf("delete: %c\n", *(str+i));
	       tmp = str+i;

	       while(*(tmp+1)){
		    *(tmp++) = *(tmp+1);
	       }
	       deleted++;
	  }
	  else{
	       i++;
	  }
     }
     /* fill up with '\0' */
     while(*(str+i)){
	  *(str+i++) = '\0';
     }
     return str;
}




char *delindex_commented(char *str, int is[], int iscount)
{
     int len = strlen(str);
     char *tmp;
     int  i = 0;
     int deleted = 0;
     
     while(i+deleted < len){
	  
	  if(hasindex(i+deleted, is, iscount))
	  {
	       printf("delete: %c\n", *(str+i));
	       tmp = str+i; /*tmp pointer for shifting */

	       /* shift chars one left to override *tmp
		* while there still is a char next to *tmp 
		*/
	       while(*(tmp+1)){
		    *(tmp++) = *(tmp+1);
	       }
	       /* count yet deleted chars to adjust index for comparison*/
	       deleted++;
	  }
	  /* incremet i if no char deleted
	   * else:  new char unter same index after deletion
	   * (so no increment required)
	   */
	  else{
	       i++;
	  }
     }
     /* fill up with '\0' */
     while(*(str+i)){
	  *(str+i++) = '\0';
     }
     return str;
}



int main(int argc, char **argv)
{
     char *res;
     
     int is[] = {6,0,1,2};
     int iscount = sizeof(is)/sizeof(int);
     
     if(argc >= 2){
	  res = delindex(argv[1], is, iscount);
	  printf("res: %s\n", res);
	  
     }
     else{

     }
     
     return 0;
}


