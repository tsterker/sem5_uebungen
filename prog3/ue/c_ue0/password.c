#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>





void print_curr(char *p1, char *p2, int i)
{
     int ti = 0;
     while(ti < i){
	  putchar(*(p1+ti));
	  ti++;
     }
     printf("\n");
     ti = 0;
     
     while(ti < i){
	  putchar(*(p2+ti));
	  ti++;
     }
     printf("\n");
}

int check_eq_next_n(char *p1, char *p2, int n)
{
     if(*(p2+n)) return 0;
     while(n && *p2){
	  if(*p1++ != *p2++) return 0;
	  n--;
     }
     return 1;
}


int is(char *pw)
{
/* "global" start/end of substrings to compare - outer loop */
     char *s = pw, *e = pw+1;  
/* "local" start/end of substrings to compare - inner loop*/
     char *p1, *p2;           
/* difference between local start/end */
     int diff;
     int i = 0;
     int len = strlen(pw);
     int chars = 1;
     p1 = s;
     p2 = e;
     while(*e){
	  p1 = s;
	  p2 = e;
	  diff = p2-p1;
	  while(*p2 && diff <= len/2){
	       printf("------------------\n");
	       if(check_eq_next_n(p1, p2, chars))
		    return 0;
	       p1++;
	       p2++;
	       i++;
	  }
	  chars++;
	  e++;
     }
     return 1;
}


int main(int argc, char **argv)
{
     int save;
     

     if(argc == 2){
	  
	  save = is(argv[1]);
	  
	  if(save)
	       printf("SAVE\n");
	  else
	       printf("NOT SAVE\n");
     }
     else{
	  

     }
     
     
     return 0;
}


