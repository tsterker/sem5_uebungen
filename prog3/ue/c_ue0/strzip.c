#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>


char *zip(char *s1, char *s2)
{
     char *tr, *t1=s1, *t2=s2;
     int len = strlen(s1)+strlen(s2)+1; /* +1 for terminating '\0' */
     char *ret = (char*)malloc(len*sizeof(char));
     tr = ret;
     while(*s1 || *s2){
	  if(*s1)
	       *ret++ = *s1++;
	  if(*s2)
	       *ret++ = *s2++;
     }
     *ret = '\0';
/*
     free(s1);
     free(s2);
*/
     return tr;
}




int main(int argc, char **argv)
{
     char *str;
     
     if(argc > 2){
	  str = zip(argv[1], argv[2]);
	  printf("zip(%s, %s)\n", argv[1], argv[2]);
	  printf("%s\n", str);
	  free(str);
     }
     else{

     }
     
     return 0;
}


