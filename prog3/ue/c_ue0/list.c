#include <stdio.h>

#include <stdlib.h>
#include <string.h>
#include <assert.h>



struct node
{
     char *str;
     struct node *next;

};

typedef struct node *pnode;


pnode add(pnode root, char *str)
{
     pnode tmp = root;
     pnode new = (pnode)malloc(sizeof(struct node));
     new->str = (char*)malloc(strlen(str)*sizeof(char)+1);
     strcpy(new->str, str);
     new->next = NULL;
     
     if(!root)
	  return new;
     
     while(tmp->next){
	  tmp = tmp->next;
     }
     tmp->next = new;
     return root;
}

pnode add_first(pnode root, char *str)
{
     pnode tmp = root;
     pnode new = (pnode)malloc(sizeof(struct node));
     new->str = (char*)malloc(strlen(str)*sizeof(char)+1);
     strcpy(new->str, str);
     new->next = NULL;
     
     root = new;
     root->next = tmp;
     return root;
}

pnode add_ith(pnode root, char *str, int i)
{
     pnode parent=root, tmp = root;
     pnode new = (pnode)malloc(sizeof(struct node));
     new->str = (char*)malloc(strlen(str)*sizeof(char)+1);
     strcpy(new->str, str);
     new->next = NULL;

     if(i < 0){
	  free(new->str);
	  free(new);
	  return root;
     }
     if(0 == i)
	  return add_first(root, str);
     

     while(0 < i--){
	  if(!tmp){
	       free(new->str);
	       free(new);
	       return root;
	  }
	  
	  parent = tmp;
	  tmp = tmp->next;
     }

     parent->next = new;
     new->next = tmp;
     return root;
}


pnode del_last(pnode root)
{
     pnode parent=root, tmp = root;

     while(tmp->next){
	  parent = tmp;
	  tmp = tmp->next;
     }
     parent->next = NULL;
     free(tmp->str);
     free(tmp);

     return root;
}

pnode del_first(pnode root)
{
     pnode ret = root->next;
     free(root->str);
     free(root);
     return ret;
}




pnode del_ith(pnode root, int i)
{
     pnode parent=root, tmp = root;
     
     if( i < 0)
	  return root;
     
     if(0 == i)
	  return del_first(root);
     
     while(0 < i--){
	  if(!tmp)
	       return root;
	  parent = tmp;
	  tmp = tmp->next;
     }
     
     parent->next = tmp->next;
     free(tmp->str);
     free(tmp);
     return root;
}


void printl(pnode root)
{
     while(root){
	  printf("%s -> ", root->str);
	  root = root->next;
     }
     printf(" (NULL)\n");
     
}

void clear(pnode root)
{
     pnode tmp = root;
     
     while(root->next){
	  tmp = root;
	  root = root->next;
	  free(tmp->str);
	  free(tmp);
     }
     
     free(root->str);
     free(root);
}

pnode reverse_in_place(pnode root)
{
     pnode new_root = root;
     pnode next = root->next;
     
     if(!root->next)
	  return root;

     new_root = reverse_in_place(root->next);

     next->next = root;
     
     if(root->next->next == root)
	  root->next = NULL;
     return new_root;
}


pnode rip(pnode root)
{
     pnode new_root = root, next = root->next;
     

     if(!next) return root;
     
     new_root = rip(root->next);
     
     if(root->next->next = root)
	  root->next = NULL;
     
     return new_root;
     
}

pnode printr(pnode root)
{
     if(!root) return root;
     printr(root->next);
     printf("(<-- %s)", root->str);
     return root;
}


int main(int argc, char **argv)
{
     int i;
     pnode root = NULL;

     if(argc >= 2){
	  
	  for(i = 1; i < argc; i++){
	       root = add(root, argv[i]);
	  }
	  printl(root);
	  printf("del_last\n");
	  root = del_last(root);
	  printl(root);

	  printf("del_first\n");
	  root = del_first(root);
	  printl(root);

	  printf("del_ith(1)\n");
	  root = del_ith(root, 1);
	  printl(root);

	  printf("del_ith(99)\n");
	  root = del_ith(root, 99);
	  printl(root);

	  printf("del_ith(-1)\n");
	  root = del_ith(root, -1);
	  printl(root);

	  printf("del_ith(4)\n");
	  root = del_ith(root, 4);
	  printl(root);

	  printf("del_ith(0)\n");
	  root = del_ith(root, 0);
	  printl(root);

	  printf("add_first('LOL')\n");
	  root = add_first(root, "LOL");
	  printl(root);

	  printf("add_ith('ROFL, 1')\n");
	  root = add_ith(root, "ROFL", 1);
	  printl(root);
	  
	  printf("add_ith('OMG, 99')\n");
	  root = add_ith(root, "OMG", 99);
	  printl(root);
	  
	  printf("add_ith('OMG, -1')\n");
	  root = add_ith(root, "OMG", -1);
	  printl(root);
	  
	  printf("add_ith('GAGA, 0')\n");
	  root = add_ith(root, "GAGA", 0);
	  printl(root);

	  printf("reverse_in_place\n");
	  root = reverse_in_place(root);
	  printl(root);

	  printf("rip\n");
	  root = rip(root);
	  printl(root);
	  printf("print reverse:\n");
	  printr(root);


	  
	  clear(root);
     }
     else{

     }
     
     return 0;
}


