#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>


struct lnode
{
     char *str;
     struct lnode *next;
};

typedef struct lnode *plnode;
     

struct tnode
{
     char *str;
     int count;
     struct tnode *left;
     struct tnode *right;

};

typedef struct tnode *ptnode;




ptnode add(ptnode root, char *str)
{
     int diff;
     ptnode parent, tmp = root;
     ptnode new = (ptnode)malloc(sizeof(struct tnode));
     new->str = (char*)malloc(strlen(str)*sizeof(char)+1); /* +1 for terminating '\0' */
     strcpy(new->str, str);
     new->count = 1;
     new->left = NULL;
     new->right = NULL;

     if(!root)
	  return new;
     
     while(tmp){
	  parent = tmp;
	  diff = strcmp(new->str, tmp->str);
	  if(0 == diff){
	       tmp->count++;
	       free(new->str);
	       free(new);
	       return root;
	  }
	  else if(diff < 0){
	       tmp = tmp->left;
	  }
	  else{
	       tmp = tmp->right;
	  }
     }

     if(diff < 0){
	  parent->left = new;
     }
     else{
	  parent->right = new;
     }
     return root;
}


/* traverse binary search tree with left elemens smaller */
ptnode traverse(ptnode root)
{
     if(!root) return root;
     /* print here for smallest first */
     traverse(root->right);
     printf("tnode->str == %s\n", root->str);
     /* print here for biggest first */
     /* ... */
     return traverse(root->left);
}

ptnode clear(ptnode root)
{
     if(!root)
	  return root;
     clear(root->left);
     clear(root->right);

     free(root->str);
     free(root);

     return root;
}

plnode add_to_list(plnode root, char *str)
{
     plnode tmp = root;
     plnode new = (plnode)malloc(sizeof(struct lnode));
     new->str = (char*)malloc(strlen(str)*sizeof(char)+1);
     strcpy(new->str, str);
     new->next = NULL;
     
     if(!root)
	  return new;
     
     while(tmp->next){
	  tmp = tmp->next;
     }
     
     tmp->next = new;
     
     return root;
}



plnode tolist(ptnode root, plnode list)
{
     if(!root) return list;
				 
     tolist(root->left, list);
     tolist(root->left, list);
     list = add_to_list(list, root->str);
     tolist(root->right, list);

     return list;
}

void printlist_reverse(plnode l)
{
     if(l)
	  printlist_reverse(l->next);
     /* to implement */
     

}

void printlist(plnode l)
{
     
     while(l){
	  printf("%s --> ", l->str);
	  l = l->next;
     }
     printf("\n");

}




int main(int argc, char **argv)
{
     int i;
     ptnode root = NULL;
     plnode list = NULL;
     
     

     if(argc >= 2){
	  for(i = 1; i < argc; i++){
	       root = add(root, argv[i]);
	  }
	  traverse(root);
	  printf("----------------------\n");


	  
	  list = tolist(root, list);
	  printlist(list);
	  
	  clear(root);
	  
     }
     else{

     }
     
     return 0;
}


