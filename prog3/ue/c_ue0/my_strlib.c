#include <stdio.h>
#include <stdlib.h>
#include <string.h>


char *mystrcat(char *dest, const char *src);
char *mystrchr(char *s, int c);
int mystrcmp(const char *s1, const char *s2);


char *mystrchr(char *s, int c)
{
     int i = 0;
     
     while(*(s+i) != c){
	  i++;
     }

     return s+i;
}




char *mystrcat(char *dest, const char *src)
{
     char *ret = dest;
     while(*dest){
	  dest++;
     }
     while(*src){
	  *dest++ = *src++;
     }
     *dest = '\0';
     return ret;
}


int mystrcmp(const char *s1, const char *s2)
{
     int diff;
     
     while(*s1 && *s2){
	  diff = *s1 - *s2;
	  if(diff != 0)
	       return diff;
	  s1++;
	  s2++;
	  
     }

     return diff;
}



int main(int argc, char **argv)
{

     char *str = (char*)malloc(100*sizeof(char));
     char *c;
     int i;
     
     
     
     if(argc > 2){
	  
	  strcpy(str, argv[1]);
	  printf("strcat(%s, %s)\n", str, argv[2]);
	  str = mystrcat(str, argv[2]);
	  printf("%s\n\n", str);

	  printf("strchr(%s, o):\n", str);
	  c = mystrchr(str, 'o');
	  printf("found: %s\n\n", c);

	  printf("strcmp(%s, %s):\n", str, "hallo dude");
	  i = mystrcmp(str, "hallo dude");
	  printf("result: %d\n\n", i);

	  printf("strcmp(%s, %s):\n", str, "hallo duda");
	  i = mystrcmp(str, "hallo duda");
	  printf("result: %d\n\n", i);



	  
	  
	  
     }
     else{
	  

     }
     
     
     return 0;
}


