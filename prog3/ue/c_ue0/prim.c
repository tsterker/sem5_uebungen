#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


int isprime(int n)
{
     int tmp = n;
     if(n < 2) return 0;
     while(tmp-- && tmp > 1) if( ! ( n % tmp) ) return 0;
     return 1;
}



int main(int argc, char **argv)
{
     int i, x;
     
     
     
     if( argc != 2){
	  for(i = 0; i < 100; i++){
	       if(isprime(i))
		    printf("%d\n", i);
	  }
     }
     else{
	  x = atoi(argv[1]);
	  printf("%d %s\n", x, (isprime(x)? "PRIME" : "NO PRIME"));
     }
     
     return 0;
}


