#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>



/* matches the beginning of str with substr? */
int is_substr(char *str ,char *sub)
{
     while(*str && *sub){
	  if(*str++ != *sub++)
	       return 0;
     }
     if(*sub) /* if sub didnt fully match */
	  return 0;
     return 1;
}

/* does the word contain the substr ? */
int contains_substr(char *str, char *sub)
{
     while(*str){
	  if(is_substr(str, sub))
	     return 1;
	  str++;
     }
     return 0;
}

/* deletes string at index i from array
 * and shifts the other strings in array accordingly
 */
void delete_ith_str(char **a, int i, int asize)
{
     int x = 0;
     char *tmp;
     char *zero;
     zero = (char*)malloc(sizeof(char)+1);
     strcpy(zero, ".");

     /* index to big */
     if(i >= asize)
	  return;

     /* move forward to index to delete */
     if(x <= i)
	  tmp = a[x]; /* pointer eventually points to str to be deleted - for freeing */
	  
     while(x < i){
	  x++;
	  tmp = a[x]; /* pointer eventually points to str to be deleted - for freeing */
     } /* arrived at str to delete */

     
     while(x+1 < asize){
	  a[x] = a[x+1];
	  x++;
     }

     a[x] = zero;
     free(tmp);
}



/* deletes all strings from array which have substring
 * and rearrange array accordingly.
 * size of str array has to be known.
 */
char **stradel(char **a, char *sub, int asize)
{
     int i = 0;
     
     while(i < asize){
	  printf("(stradel) a[%d] == %s\n", i, a[i]);
	  if(contains_substr(a[i], sub))
	       delete_ith_str(a, i, asize);
	  else
	       i++;
     }
     return a;
     
}


void clearstra(char **a, int asize)
{
     int i = 0;
     while(i < asize){
	  if(a[i])
	       free(a[i]);
	  i++;
     }
     free(a);
}


int main(int argc, char **argv)
{
     char **a;
     int i;
     int b = 0;
     int asize;
     
     if(argc > 2){

	  /* initialize string array */
	  asize = argc-2;
	  a = (char**)malloc(asize*sizeof(char*));
	  for(i = 0; i < asize; i++){
	       a[i] = (char*)malloc(strlen(argv[i+1])*sizeof(char)+1);
	       strcpy(a[i], argv[i+1]);
	       
	  }

	  /* print array values + substr to search for*/
	  printf("array:\n");
	  printf("----------------\n");
	  for(i = 0; i < asize; i++){
	       printf("%s\n", a[i]);
	  }
	  printf("(substring: %s)\n", argv[argc-1]);
	  printf("----------------\n");
	  
	  a = stradel(a, argv[argc-1], asize);

	  printf("new array:\n");
	  printf("----------------\n");
	  for(i = 0; i < asize; i++){
	       printf("%s\n", a[i]);
	       
	  }
	  
	  
	  clearstra(a, asize);
	  
	  
	  
     }
     else{

     }
     
     return 0;
}


