#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>



unsigned int calc(unsigned int u1, unsigned int u2, char *op)
{
     switch(*op){
     case 'o': return u1 | u2;
     case 'a': return u1 & u2;
     case 'x': return u1 ^ u2;
     case 'l': return u1 << u2;
     case 'r': return u1 >> u2;
     }

     assert(0);
     return -1;
}



int mystrlen(char *s)
{
     int i = 0;
     while(*s++){
	  i++;
     }
     return i;
}


/* get bit value for index i
 * ...16 8 4 2 1
*/
int bval(int i)
{
     int ret = 1;
     if(0==i)
	  return ret;
     
     while(i){
	  ret *= 2;
	  i--;
     }
     return ret;
}

int mypow(int x, int n)
{
     int ret = 1;
     while(0 < n--){
	  ret *= x;
     }
     return ret;
}

unsigned int str_to_uint(char *s)
{
     int len = mystrlen(s)-1;
     unsigned int u = 0;
     while(*s){
	  if(*s-'0') /* if true, *s == 1 */
	       u += bval(len);
	  len--;
	  s++;
     }
     return u;
}


void print_dec_as_bin(unsigned int u)
{
/* byte_count * bit_count - 1 (because initial at first bit)*/
     int x= sizeof(unsigned int)*8-1; 
     int first_one = 0;
     
     printf("%u == ", u);

     while(x+1){
	  if(u & 1<<x--){
	       putchar('1');
	       first_one = 1;
	  }
	  else if(first_one)
	       putchar('0');
	  
     }
     printf("\n");
     
}



int main(int argc, char **argv)
{
     unsigned int u1, u2;
     char *op;
     
     
     str_to_uint(argv[1]);

     if(4 == argc){
	  u1 = str_to_uint(argv[1]);
	  u2 = str_to_uint(argv[3]);
	  op = argv[2];
	  print_dec_as_bin(u1);
	  print_dec_as_bin(u2);
	  
	  
     }
     else{
	  printf("zu wenige Parameter. 3 erwartet.\n");
     }
     
     return 0;
}


