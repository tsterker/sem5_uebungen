#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


int highest_count = 0;

struct lnode
{
     char *str;
     struct lnode *next;
};

typedef struct lnode *plnode;
     

struct node
{
     char *str;
     int count;
     int lvl;
     struct node *left;
     struct node *right;
};

typedef struct node *pnode;

int empty(pnode root);
pnode add(pnode root, char *str);
pnode del(pnode root, char *str);
void clear(pnode root);
pnode back(pnode root, void (*func)(pnode));
void print_highest(pnode node);
pnode traverse(pnode root, void (*func)(pnode));



int empty(pnode root)
{
     return (root == NULL);
}


pnode add(pnode root, char *str)
{
     int lvl;
     pnode tmp = root;
     pnode parent;
     pnode new = (pnode)malloc(sizeof(struct node));
     new->str = (char *)malloc(strlen(str)*sizeof(char)+1); /* +1 for terminating \0 */
     new->lvl = 0;
     strcpy(new->str, str);
     
     if(highest_count > 1) highest_count = 1;
          
     new->count = 1;
     new->left = NULL;
     new->right = NULL;
     
     if(empty(root)){
	  root = new;
	  printf("new->str (FIRST) ::  %s\n", new->str);
	  printf("lvl == %d\n", new->lvl);
	  return root;
     }

     lvl = 1;
     int cmp;
     while(tmp){
	  parent = tmp;
	  new->lvl = lvl;

	  cmp = strcmp(str, tmp->str);

	  if(0 == cmp){
	       tmp->count++;
	       if(tmp->count > highest_count)
		    highest_count = tmp->count;
	       printf("new->str (cmp==0) ::  %s\n", new->str);
	       printf("lvl == %d\n", new->lvl);
	       free(new->str);
	       free(new);
	       return root;
	  }
	  if(0 > cmp){
	       tmp = tmp->left;
	  }
	  else{
	       tmp = tmp->right;
	  }
	  lvl++;
     }
     printf("new->str (LAST) ::  %s\n", new->str);
     printf("lvl == %d\n", new->lvl);
     

     parent->left = (cmp < 0)? new : parent->left;
     parent->right = (cmp > 0)? new : parent->right;

         
     return root;
}

void clear(pnode node)
{
     printf("--DELETE---node: %s\n", node->str);

     free(node->str);
     free(node);
}

void print_highest(pnode node)
{
     if(node->count >= highest_count){
	  printf("-HIGHEST: %s\n", node->str);
	  printf("count: %d\n", node->count);
	  printf("lvl: %d\n", node->lvl);

     }
}

void print_node(pnode node)
{
     printf("lvl %d : %s\n", node->lvl, node->str);
     printf("count: %d\n", node->count);
}


pnode traverse(pnode root, void (*func)(pnode))
{
     pnode tmp = root;
     if(!root) return root;
     /* call func here for smaller nodes first */
     func(root);
     while( (tmp = traverse(tmp->right, func)) ){ ;  }
     /* call func here for bigger nodes first */
     /* ... */
     return traverse(root->left, func);
}

/* traverse for list making */
pnode ltrav(pnode root, plnode l, void (*func)(pnode))
{
     pnode tmp = root;
     plnode tl = (plnode)malloc(sizeof(struct lnode));
     tl->str = (char*)malloc(strlen(root->str)*sizeof(char)+1);
     tl->next = NULL;
     
     if(!root) return root;
     /* call func here for smaller nodes first */

     strcpy(tl->str, root->str);
     if(l == NULL){
	  l = tl;
     }
     else{
	  while(l->next){
	       l = l->next;
	  }
	  l->next = tl;
     }
/* 
   func(root);
 */
     
     while( (tmp = ltrav(tmp->right,l, func)) ){ ;  }
     /* call func here for bigger nodes first */
     /* ... */
     return ltrav(root->left,l, func);
}



int main(int argc, char **argv)
{
     int i;
     pnode root = NULL;
     plnode list = NULL;

     if(argc > 1){

	  for(i=1; i<argc; i++){
	       root = add(root, argv[i]);
	  }
	  traverse(root, print_node);
	  traverse(root, clear);
     }
     else{


	  root = add(root, "5");
	  root = add(root, "4");
	  root = add(root, "2");
	  root = add(root, "3");
	  root = add(root, "9");
	  root = add(root, "6");
	  root = add(root, "7");
	  root = add(root, "1");
	  root = add(root, "8");
	  printf("------------------------------\n");

	  ltrav(root, list, clear);
	  traverse(root, clear);
     }
     
     
     
     return 0;
}


