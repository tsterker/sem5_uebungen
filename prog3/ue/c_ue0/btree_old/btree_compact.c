#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int flag = 0;

struct node
{
     char *str;
     int count;
     int lvl;
     struct node *left;
     struct node *right;
};

typedef struct node *pnode;

int empty(pnode root);
pnode add(pnode root, char *str);
pnode del(pnode root, char *str);
void clear(pnode root);

int empty(pnode root)
{
     return (root == NULL);
}




pnode add(pnode root, char *str)
{
     int lvl;
     pnode tmp = root;
     pnode parent;
     pnode new = (pnode)malloc(sizeof(struct node));
     new->str = (char *)malloc(strlen(str)*sizeof(char)+1); /* +1 for terminating \0 */
     new->lvl = 0;
     strcpy(new->str, str);
     
     new->count = 1;
     new->left = NULL;
     new->right = NULL;
     
     if(empty(root)){
	  root = new;
	  printf("new->str (FIRST) ::  %s\n", new->str);
	  printf("lvl == %d\n", new->lvl);
	  return root;
     }

     lvl = 1;
     int cmp;
     while(tmp){
	  parent = tmp;
	  new->lvl = lvl;

	  cmp = strcmp(str, tmp->str);

	  if(0 == cmp){
	       tmp->count++;
	       printf("new->str (cmp==0) ::  %s\n", new->str);
	       printf("lvl == %d\n", new->lvl);
	       return root;
	  }
	  if(0 > cmp){
	       tmp = tmp->left;
	  }
	  else{
	       tmp = tmp->right;
	  }
	  lvl++;
     }
     printf("new->str (LAST) ::  %s\n", new->str);
     printf("lvl == %d\n", new->lvl);
     

     parent->left = (cmp < 0)? new : parent->left;
     parent->right = (cmp > 0)? new : parent->right;

         
     return root;
}

void clear(pnode node)
{
     free(node->str);
     free(node);
}

void nix(pnode node)
{
     printf("-----node: %s\n", node->str);
     printf("lvl: %d\n", node->lvl);
}



pnode traverse(pnode root, void (*func)(pnode))
{
     pnode ret, tmp = root;
     if(!root) return root;
     while((tmp = traverse(tmp->left, func))){; }
     ret = traverse(root->right, func);
     func(root);
     return  ret;
}

int main(int argc, char **argv)
{
     int i;
     pnode root = NULL;

     if(argc > 1){

	  for(i=1; i<argc; i++){
	       root = add(root, argv[i]);
	  }
	  traverse(root, nix);
	  traverse(root, clear);
     }
     else{

	  root = add(root, "5");
	  root = add(root, "4");
	  root = add(root, "2");
	  root = add(root, "3");
	  root = add(root, "9");
	  root = add(root, "6");
	  root = add(root, "7");
	  root = add(root, "1");
	  root = add(root, "8");
	  printf("------------------------------\n");
	  traverse(root, nix);
	  traverse(root, clear);
     }
     
     
     
     return 0;
}


