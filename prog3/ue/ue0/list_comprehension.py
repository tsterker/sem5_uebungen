


# finding all primes in n

# UNEFFECTIVE
prim = lambda n: [j for j in range(2,n) if j not in [i for i in range(2,n) for x in range(2,n) if i != x and not i%x]]

print "prim(20):\n",prim(20)

# EFFECTIVE
isprim = lambda n: 0 == sum([(n%i == 0) for i in range(2,n)])

print "-- is prim --"
for p in range(2,1000):
    if isprim(p):
        print "",p," is prime"
