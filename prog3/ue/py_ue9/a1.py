#! /usr/bin/python

import sys
import string

LETTERS = string.lowercase + string.uppercase

if __name__ == '__main__':
    try:
        for l in open(sys.argv[1]).readlines():
            for c in LETTERS:
                if l.find(c) >= 0:
                    print "CONTAINS"
#                    print l
                else:
                    print "..."
#            print l.strip()
    except IndexError, e:
        print "Usage:"
        print sys.argv[0]," <filename>"
    except IOError, e:
        print "IOError: Please name an existing file."
