#include <stdio.h>
#include <stdlib.h>
#include <string.h>




struct node
{
     struct node *next;
     char *name;
};

typedef struct node *nptr;

char *reim[] = {"ene", "mene", "mu", "und", "raus", "bist", "du"};

nptr add(nptr n, char *name)
{
     int len = strlen(name);
     
     nptr ntmp= n;
     nptr new_node= (struct node *)malloc(sizeof(struct node));
     new_node->name = (char *)malloc(len*sizeof(char));
     strcpy(new_node->name, name);
     
     if(n == NULL){
	  n = new_node;
	  new_node->next = n;
     }
     else{
	  while(ntmp->next != n){
	       ntmp = ntmp->next;
	  }
	  new_node->next = n;
	  ntmp->next = new_node;
     }
     return n;
}

nptr remove_current(nptr n)
{
     nptr ntmp = n;
     
     while(strcmp(ntmp->next->name, n->name))
	  {
	       ntmp = ntmp->next;
	  }
     ntmp->next = n->next;
     free(n);
     return ntmp->next;
}

nptr abzaehlen(nptr n, int silben)
{
     while(silben > 1){
	  n = n->next;
	  silben--;
     }
     printf("----> %s\n", n->name);


     return remove_current(n);
}

void print_ring(nptr n)
{
     nptr ntmp = n;
     
     printf("(");
     while(ntmp->next != n){
	  printf("%s, ", ntmp->name);
	  ntmp = ntmp->next;
     }
     printf("%s)\n", ntmp->name);
     
}


int main(int argc, char **argv)
{
     int i = 0;
     int silben = 0;
     nptr head = NULL;
     

     if(argc >= 3 && (silben = atoi(argv[1])))
     {

	  for(i = 2; i<argc; i++){
	       head = add(head, argv[i]);
	  }
	  
	  while(head->next != head){
	       print_ring(head);
	       head = abzaehlen(head, silben);
	  }
	  printf("RESULT::\n%s\n", head->name);
	  
	  

     }

     else{
	  printf("Ungueltige Silben-Angabe!\nProgramm wird beendet.\n");
	  return 1;
     }
     
     return 0;
}

