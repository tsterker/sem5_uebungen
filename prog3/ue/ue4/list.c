#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SMAXLEN 80

struct slist
{
     struct slist *next;
     char *s;
};

struct slist *head = NULL;


void add_elem(char *str)
{
     struct slist *tmp;
     
     tmp = (struct slist *)malloc(sizeof(struct slist));
     tmp->s = (char *)malloc(strlen(str)*sizeof(char));
     
     strcpy(tmp->s, str);
     
     if(head == NULL) {
	  printf("head == NULL\n");
	  head = tmp;
	  head->next = NULL;
     }
     else{
	  printf("head != NULL\n");
	  tmp->next = head;
	  head = tmp;
     }
}


void show_list(struct slist *l)
{
     for(; l; l = l->next){
	  printf("%s\n", l->s);
     }
}


/* only positive powers allowed for now*/
int to_power(int i, int pow)
{
     int result = 1;

     while(pow > 0){
	  result *= i;
	  pow--;
     }
     return result;
}

int is_digit(char c)
{
     return ('0' <= c && c <= '9') ? 1 : 0;
}

/* ignores non-digit chars */
int str_to_int(char *str)
{
     int i = 0;
     int tmp = 0;

     int digits = 0;
     int result = 0;

     while(*(str+i) != '\0'){
	  if(is_digit(*(str+i)))
	       digits++;
	  i++;
     }

     i = 0;

     while(*(str+i) != '\0'){
	  if(is_digit(*(str+i)))
	  {
	       digits--;
	       tmp = *(str+i)-'0';
	       result += tmp*to_power(10, digits);
	  }
	  i++;
     }
     return result;
}


int main(int argc, char **argv)
{
     int i;
     
     
     if(argc >=3){
	  
	  for(i = 2; i < argc; i++){
	       add_elem(argv[i]); 
	  }
	  show_list(head);

/*
	  
*/

     }

     return 0;
}


