#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dlist.h"

void custom_print(void *v)
{
     printf(">>>>>>>>>>>>>>>>>>>Custom Print :: %s\n", (char*)v);
}


void *custom_lol(void *v)
{
     return "lol";
     
}


void testdl()
{
     dnodeptr dl = NULL;
     dnodeptr cpy = NULL;

     printf("...add(ich,bin,tim)\n");
     dl = add(dl, "ich");
     dl = add(dl, "bin");
     dl = add(dl, "tim");
     display(dl);

     printf("...add_last(sagte,ein,mensch)\n");
     dl = add_last(dl, "sagte");
     dl = add_last(dl, "ein");
     dl = add_last(dl, "mensch");
     display(dl);
     printf("...add_ith\n");
     printf("...fjeden-->1\n");
     dl = add_ith(dl, "fjeden",1);
     printf("...fall-->4\n");
     dl = add_ith(dl, "fall",4);
     printf("...x-->-1\n");
     dl = add_ith(dl, "x",-1);
     printf("...x-->99\n");
     dl = add_ith(dl, "x",99);
     display(dl);

     printf("...del\n");
     dl = del(dl);
     display(dl);

     printf("...del_ith(3)\n");
     dl = del_ith(dl,3);
     display(dl);

     printf("...del_last(3)\n");
     dl = del_last(dl);
     display(dl);

     cpy = copy(dl);
     printf("...copy..l\n");
     display(cpy);
     del_all(cpy);
     
     printf("...copy :: custom_lol\n");
     cpy = copy_custom(dl, custom_lol);
     display(cpy);
     
     printf("...del_all_custom\n");
     del_all_custom(dl,custom_print);
     printf("...del_all\n");
     del_all(cpy);
     
}


dnodeptr merge(dnodeptr links, dnodeptr rechts)
{
     dnodeptr new = NULL;
     dnodeptr li = NULL;
     dnodeptr ri = NULL;

     printf("HEYHEY\n");
     
     while(!is_empty(links) && !is_empty(rechts)){
	  li = get_ith(links,0);
	  char *lv = li->value;
	  ri = get_ith(rechts,0);
	  char *rv = ri->value;
	  
	  printf("links->value == %s\n", lv);
	  printf("rechts->value == %s\n", rv);
	  

	  int compare = strcmp(lv, rv);
	  
	  if(compare <= 0){
	       printf("li <= ri\n");
	       printf("strcmp == %d\n", compare);
	       
	       new = add_last(new, lv);
	       links = del_ith(links,0);
	  }
	  else{
	       printf("li > ri\n");

	       new = add_last(new, rv);
	       rechts = del_ith(rechts,0);
	  }
     }
     printf("HEYHEY2\n");

     while(!is_empty(links)){
	  li = get_ith(links,0);
	  new = add_last(new, li->value);
	  links = del_ith(links,0);
     }
     printf("HEYHEY3\n");

     while(!is_empty(rechts)){
	  ri = get_ith(rechts, 0);
	  new = add_last(new, ri->value);
	  rechts = del(rechts);
     }
     printf("HEYHEY4\n");

     return new;
}


dnodeptr mergesort(dnodeptr dl)
{
     dnodeptr links = NULL;
     dnodeptr rechts = NULL;
     int length = size(dl);
     int from, to;

     printf(">>>>>>>>>>>>>>>>>to split:\n");
     display(dl);
          
     if(length <= 1){
	  printf("length <= 1 :: return\n");
	  return dl;
     }

/* links */
     printf("(links):\n");
     from = 0;
     to = length/2 - 1;
     links = copy_from_to(dl, from, to);
     display(links);

/* rechts */
     printf("(rechts):\n");
     from = to+1;
     to = length-1;
     rechts = copy_from_to(dl, from, to);
     display(rechts);

/* recursion */     
     links = mergesort(links);
     rechts = mergesort(rechts);

     printf("links------------------------\n");
     display(links);
     printf("rechts------------------------\n");
     display(rechts);
     

     return merge(links, rechts);
}


int main(int argc, char **argv)
{
     int i = 0;

     if(argc > 1){
	  dnodeptr dl = NULL;
	  dnodeptr cpy = NULL;
	  dnodeptr custom_cpy = NULL;
	  dnodeptr merged = NULL;
	  for(i = 1; i<argc; i++){
	       dl = add_last(dl, argv[i]);
	  }
	  cpy = copy(dl);

	  custom_cpy = copy_custom(dl, custom_lol);
	  printf("dl:\n");
	  display(dl);
	  printf("cpy:\n");
	  display(cpy);
	  printf("custom_cpy:\n");
	  display(custom_cpy);

	  merged = mergesort(cpy);
	  display(merged);
	  printf("delete dl -----\n");
	  del_all(dl);
	  printf("delete cpy -------\n");
	  del_all(cpy);
	  printf("delete custom_cpy --------\n");
	  del_all(custom_cpy);
	  printf("delete merged --------\n");
	  del_all(merged);
	  
     }
     else{
	  printf("usage: <element>[<element>]\n");
     }
     
     
     return 0;
}

