#ifndef DLIST_H_
#define DLIST_H_


struct dnode
{
     struct dnode *next;
     struct dnode *prev;
     void *value;
};
   
typedef struct dnode *dnodeptr;

extern void display(dnodeptr dl);
extern int is_empty(dnodeptr n);
extern int size(dnodeptr n);

extern dnodeptr get_ith(dnodeptr n, int);

extern dnodeptr add(dnodeptr dl, void *v);
extern dnodeptr add_last(dnodeptr dl, void *v);
extern dnodeptr add_ith(dnodeptr dl, void *v, int);
extern dnodeptr del(dnodeptr dl);
extern dnodeptr del_last(dnodeptr dl);
extern dnodeptr del_ith(dnodeptr dl, int index);
extern void del_all(dnodeptr dl);
extern void del_all_custom(dnodeptr dl, void (*custom)(void *));
extern dnodeptr copy(dnodeptr);
extern dnodeptr copy_from_to(dnodeptr, int, int);
extern dnodeptr copy_custom(dnodeptr, void *(*custom) (void *));

#endif /* DLIST_H_ */
