#include <stdio.h>
#include <stdlib.h>
#include "dlist.h"

dnodeptr get_ith(dnodeptr dl, int index)
{
     int i = 0;

     if(index >= size(dl)){
	  return NULL;
     }

     while(dl->next != NULL && i == index){
	  i++;
	  dl = dl->next;
     }
     i++;
     return dl;
}



int size(dnodeptr dl)
{
     int i = 0;

     if(is_empty(dl)){
	  return 0;
     }

     while(dl->next != NULL){
	  i++;
	  dl = dl->next;
     }
     i++;
     return i;
}


void display(dnodeptr dl)
{
     
     while(dl->next != NULL){
	  printf("%s\n", (char*)dl->value);
	  dl = dl->next;
     }
     printf("%s\n", (char*)dl->value);

}

int is_empty(dnodeptr n)
{
     return n == NULL;
}


dnodeptr add(dnodeptr dl, void *v)
{
     dnodeptr new = (dnodeptr)malloc(sizeof(struct dnode));
     dnodeptr tdl = dl;

     new->value = v;
     new->next = NULL;
     new->prev = NULL;

     if(is_empty(dl)){
	  dl = new;
     }
     else {
	  new->next = tdl;
	  new->next->prev = new;
	  dl = new;
     }
     return dl;
}

dnodeptr add_last(dnodeptr dl, void *v)
{
     dnodeptr new = (dnodeptr)malloc(sizeof(struct dnode));
     dnodeptr tdl = dl;

     new->value = v;
     new->next = NULL;
     new->prev = NULL;

     if(is_empty(dl)){
	  dl = new;
     }
     else {
	  
	  while(tdl->next != NULL){
	       tdl = tdl->next;
	  }
	  tdl->next = new;
	  new->prev = tdl;
     }
     return dl;
}


dnodeptr add_ith(dnodeptr dl, void *v, int index)
{
     dnodeptr new = (dnodeptr)malloc(sizeof(struct dnode));
     dnodeptr tdl = dl;

     int ci = index;

     new->value = v;
     new->next = NULL;
     new->prev = NULL;

     if(0 == index){
	  return add(dl, v);
     }
     

     if(is_empty(dl)){
	  dl = new;
     }
     else {
	  while(tdl->next != NULL && ci){
	       tdl = tdl->next;
	       ci--;
	  }
	  if(ci == 0){
	       new->next = tdl;
	       new->prev = tdl->prev;
	       new->prev->next = new;
	       tdl->prev = new;
	  }
	  else{
	       printf("index %d ist ungueltig\n", index);
	       free(new);
	  }
	  
     }
     
     return dl;
}

dnodeptr del(dnodeptr dl)
{
     dnodeptr tdl = dl;
     if(is_empty(dl)) return dl;
     
     dl = dl->next;
/*     dl->prev = NULL; */
     printf("delete :: %s\n", (char *)tdl->value);
/*      free(tdl->value); */
     free(tdl);
     return dl;
}

dnodeptr del_last(dnodeptr dl)
{
     dnodeptr tdl = dl;

     if(is_empty(dl)){
	  return dl;
     }
	  
     while(tdl->next->next != NULL){
	  tdl = tdl->next;
     }
     printf("delete :: %s\n", (char *)tdl->next->value);
     free(tdl->next);
     tdl->next = NULL;
     return dl;
}


dnodeptr del_ith(dnodeptr dl, int index)
{
     dnodeptr tdl = dl;

     int ci = index;

     if(0 == index){
	  return del(dl);
     }

     if(is_empty(dl)){
	  return dl;
     }


     while(tdl->next != NULL && ci){
	  tdl = tdl->next;
	  ci--;
     }
     if(ci == 0){

	  tdl->prev->next = tdl->next;
	  tdl->next->prev = tdl->prev;
	  printf("delete :: %s\n", (char *)tdl->value);
	  free(tdl);
     }
     else{
	  printf("index %d ist ungueltig\n", index);
     }
	  
     
     return dl;
}


void del_all(dnodeptr dl)
{
     dnodeptr tmp;
   
     while(dl->next != NULL){
	  tmp = dl;
	  dl = dl->next;
	  printf("delete :: %s\n", (char *)tmp->value);
	  
	  free(tmp);
     }
     tmp = dl;
     printf("delete :: %s\n", (char *)tmp->value);
     free(tmp);
}


void del_all_custom(dnodeptr dl, void (*custom)(void *))
{
     dnodeptr tmp;
   
     while(dl->next != NULL){
	  tmp = dl;
	  dl = dl->next;
	  custom(tmp->value);
	  printf("delete :: %s\n", (char *)tmp->value);
	  free(tmp);
     }
     tmp = dl;
     custom(tmp->value);
     printf("delete :: %s\n", (char *)tmp->value);
     free(tmp);
}


dnodeptr copy(dnodeptr dl)
{
     dnodeptr cpy = NULL;
     while(dl->next != NULL){
	  cpy = add_last(cpy, dl->value);
	  
	  dl = dl->next;
     }
     cpy = add_last(cpy, dl->value) ;
     return cpy;
}

dnodeptr copy_custom(dnodeptr dl, void *(*custom) (void *))
{
     dnodeptr cpy = NULL;
     while(dl->next != NULL){
	  cpy = add_last(cpy, custom(dl->value));
	  
	  dl = dl->next;
     }
     cpy = add_last(cpy, custom(dl->value));
     return cpy;
}


dnodeptr copy_from_to(dnodeptr dl, int from, int to)
{
     dnodeptr cpy = NULL;
     int i;
     int length = size(dl);
     int diff = 0;
     printf("------------------\n");
     printf("cpy_from_to :: (from,to) == ( %d, %d)\n", from, to);

     if(from > to || from < 0 || to < 0 || from >= length || to >= length){
	  printf("invalid 'from' AND/OR 'to' index");
	  return NULL;
     }
     i = from;
     while(i > 0){
	  dl = dl->next;
	  i--;
     }
     
     diff = to-from;
     
     while(diff > 0){
	  cpy = add_last(cpy, dl->value);
	  dl = dl->next;
	  diff--;
     }
     cpy = add_last(cpy, dl->value);
     printf("------------------\n");
     
     
     return cpy;
}



