#! /usr/bin/python

from PIL import Image, ImageDraw
from obj_parser import objread, objread_tri
import sys

modes = {"xy":(0,1), "xz":(0,2), "yz":(1,2)}
FILETYPE = "png"


def machebild(pts, filename="bild", mode="xy"):
    pp = lambda t: im.putpixel(t, 1)
    im = Image.new("1", (400,400))


    p0,p1 = modes.get(mode, (None, None))
    if p0 and p1:
        print "Mache alle bilder im mode: ", mode
        map(pp, [(int(p[p0]),int(p[p1])) for p in pts])
        im.save(filename+"_"+mode+"."+FILETYPE)
    else:
        print "Mache alle bilder"
        for key, value in modes.items():
            map(pp, [(int(p[value[0]]),int(p[value[1]])) for p in pts])
            im.save(filename+"_"+key+"."+FILETYPE)

def machebild_tri(ptri, filename="bild_tri", mode="xy"):
    tris = ptri[1]
    im = Image.new("1", (400,400))
    draw = ImageDraw.Draw(im)
    dp = lambda l: draw.polygon(l, outline=128)

    p0,p1 = modes.get(mode, (None, None))
    if p0 and p1:
        print "Mache bilder im mode: ", mode
        map(dp, [[ (int(p[p0]),int(p[p1])) for p in t] for t in tris])
        im.save(filename+"_"+mode+"."+FILETYPE)
    else:
        print "Mache alle bilder"
        for key, value in modes.items():
            map(dp, [[ (int(p[value[0]]),int(p[value[1]])) for p in t] for t in tris])
            im.save(filename+"_"+key+"."+FILETYPE)



if __name__ == '__main__':
    if len(sys.argv) > 1:
        filename = sys.argv[1].replace(".obj", "")
        try:
            pts = objread(filename+".obj")
            machebild(pts, filename, "all")

            ptri = objread_tri(filename+".obj")
            machebild_tri(ptri, filename+"_tri", "all")
            print ptri

        except IOError:
            print "obj-file not found!"

    else:
        print "usage:"
        print sys.argv[0], "<obj-filename>"
