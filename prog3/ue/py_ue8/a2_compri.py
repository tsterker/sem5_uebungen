

import string
import sys
lower = string.ascii_lowercase
upper = string.ascii_uppercase


if __name__ == '__main__':
    if len(sys.argv) > 1:
        prev = None
        for arg in sys.argv[1:]:
            s = ""
            c = 1
            for char in arg:
                if prev == char:
                    c += 1
                else:
                    prev = prev if prev else ""
                    s = ''.join([s, prev, str(c)]) if c > 1 else ''.join([s,prev])
                    c = 1
                prev = char
            s = ''.join([s, prev, str(c)]) if c > 1 else ''.join([s,prev])
            prev = ""
            print s
