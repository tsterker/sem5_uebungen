#! /usr/bin/python

import string
import sys

def objread(filename):
    pts = []
    for line in open(filename, 'r'):
        if not line.strip() or line[0] == '#':
            continue

        v = line.find("v")
        if v == 0:
            line = line.replace("v", "").strip()
            t = tuple(map(float, line.split()))
            pts.append(t)
    return pts

def objread_tri(filename):
    pts = []
    tri_indexes = []
    for line in open(filename, 'r'):
        if not line.strip() or line[0] == '#':
            continue

        mode = "v" if line.find("v") == 0 else "f"
        cast = float if mode == "v" else int
        line = line.replace(mode, "").strip()
        t = tuple(map(cast, line.split()))
        if mode == "v":
            pts.append(t)
        else:
            tri_indexes.append(t)

        # get triangle points
        tris = []
        for indexes in tri_indexes:
            tri = tuple([pts[j] for j in indexes])
            tris.append(tri)

    return (pts, tris)



if __name__ == '__main__':
    filename = "test.obj"
    if len(sys.argv) > 1:
        filename = sys.argv[1]

    l = objread(filename)
    print "Points read from file",filename,":"
    print l
