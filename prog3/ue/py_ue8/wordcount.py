
import sys


def words(l):
        d = {}
        for w in l:
            d[w] = d.get(w, 0)+1

        maxcount = max(d.values())
        l = []
        for key,value in d.items():
            if value == maxcount:
                l.append(key)

        for elem in sorted(l):
            print maxcount,":",elem



if __name__ == '__main__':
    if len(sys.argv) > 1:
        words(sys.argv[1:])
